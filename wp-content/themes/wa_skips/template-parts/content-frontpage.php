<?php
global $opt_settings;
/**
 * Template part for displaying Home page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wa_skips
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">
		<div class="content-box text-center">
			<?php the_content(); ?>
		</div>
		<div class="content-info text-center">
			<div class="row">
			  	<?php for($i=1; $i<4; $i++): $icon = $opt_settings['opt-info-icon-'.$i]['url']; $icon_title = $opt_settings['opt-info-title-'.$i]; $icon_desc = $opt_settings['opt-info-desc-'.$i]; ?>
			    <div class="col-sm-4">
			      <div class="info-box">
			        <span><img src="<?php echo $icon; ?>" alt=""></span>
				    <h2><?php echo $icon_title; ?></h2>
				    <p><?php echo $icon_desc; ?></p>
			      </div>
			    </div>
			    <?php endfor; ?>
			</div>
		</div>
	</div>
	<div class="bg-grey">
		<div class="container">
			<div class="content-call text-white flex center-vertical">
				<span class="img-call"><img src=" <?php bloginfo('template_directory')?>/assets/images/phone.png" alt="phone"></span>
				<span style="display: inline-block; margin-left: 40px;">
					<p style="font-size: 38px; font-weight: 800; padding-top:4px;">Call us on : <a href="tel:<?= str_replace(' ', '', do_shortcode ('[phone-number]')); ?>"><span class="text-green"><?= do_shortcode('[phone-number]') ?></span></a></p>
					<p style="font-size: 33px;">Get the best price today and guarantee next day delivery</p>
				</span>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="content-box text-center">
			<?= CFS()->get('waste_type'); ?>
		</div>
	</div>
</div><!-- #post-<?php the_ID(); ?> -->