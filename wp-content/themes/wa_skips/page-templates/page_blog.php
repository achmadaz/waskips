<?php
  /**
  * Template Name: Blog
  */
get_header();
$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>5));
?>
</div>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div class="container">
    <?= the_breadcrumb(); ?>
    <div class="entry-content">
      <!-- the loop -->
      <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
        <div class="wrap-blog-content">
          <div class="row">
            <div class="col-sm-5">
              <?php if (has_post_thumbnail( $post->ID ) ): ?>
                  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
              <div class="blog-img" style="background-image: url('<?php echo $image[0]; ?>')"></div>
              <?php endif; ?>
            </div>
            <div class="col-sm-7">
              <div class="blog-text">
                <h2><a href="<?php the_permalink(); ?>" class="text-grey"><?php the_title(); ?></a></h2>
                <span><?= the_time('Y/m/d');?></span>
                <p><?= get_the_excerpt(); ?></p>
                <a href="<?php the_permalink(); ?>"><?= do_shortcode('[read-more]'); ?></a>
              </div>
            </div>
          </div>
        </div>
      <?php endwhile; ?>
      <!-- end of the loop -->
    </div>
  </div>
</article>

<?php
get_footer();
