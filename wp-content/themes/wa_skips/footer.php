<?php global $opt_settings;
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wa_skips
 */

?>

	</div><!-- #content -->

	<footer id="colophon">
		<div class="bg-grey">
			<div class="container">
				<div class="content-call text-white flex center-vertical pd-30">
					<ul class="taglines text-center">
						<?php foreach($opt_settings['opt-taglines'] as $taglines): ?>
						<li><span style="font-size: 24px" class="fa fa-star text-green"></span> <?= $taglines ?></li>
						<?php endforeach ?>
						<li><span style="font-size: 24px" class="fa fa-star text-green"></span></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="site-footer line">
			<div class="container">
				<div class="site-info">
					<div class="col-sm-3">
						<div class="footer-logo flex center-vertical">
							<img src="<?= $opt_settings['opt-logo']['url'];?>" alt="Logo" class="img-responsive">
							<div class="social flex center-vertical">
								<ul>
									<?php if ($opt_settings['opt-enable-facebook']): ?>
										<li><a target="_blank" href="<?= $opt_settings['opt-facebook-url']; ?>" class="fa fa-facebook-square"></a></li>
									<?php endif; ?>
									<?php if ($opt_settings['opt-enable-twitter']): ?>
										<li><a target="_blank" href="<?= $opt_settings['opt-twitter-url']; ?>" class="fa fa-twitter-square"></a></li>
									<?php endif; ?>
									<?php if ($opt_settings['opt-enable-google-plus']): ?>
										<li><a target="_blank" href="<?= $opt_settings['opt-google-plus-url']; ?>" class="fa fa-google-plus-square"></a></li>
									<?php endif; ?>
									<?php if ($opt_settings['opt-enable-instagram']): ?>
										<li><a target="_blank" href="<?= $opt_settings['opt-instagram-url']; ?>" class="fa fa-instagram"></a></li>
									<?php endif; ?>
								</ul>
							</div>
							<div class="payment">
								<h3>PAYMENT METHOD</h3>
								<img src="<?= $opt_settings['opt-payment-icon']['url'];?>" alt="">
							</div>
						</div>
					</div>
					<?php dynamic_sidebar( 'footer-1' ); ?>
					<?php dynamic_sidebar( 'footer-2' ); ?>
					<div class="col-sm-3">
						<div class="contact-us flex center-vertical">
							<h3>CONTACT US</h3>
							<ul>
								<li><span class="fa fa-map-marker text-green"></span> <?= do_shortcode('[location-address]') ?></li>
								<li><span class="fa fa-phone text-green"></span> <?= do_shortcode('[phone-number]') ?></li>
								<li><span class="fa fa-envelope text-green"></span> <?= do_shortcode('[email-address]') ?></li>
							</ul>
						</div>
					</div>
				</div><!-- .site-info -->
			</div>
		</div>
		<div class="copyright text-center">
			<div class="container">
				<?= $opt_settings['opt-copyright'] ?>. Website design by <a href="https://www.somsweb.com.au" target="_blank">SOMS</a>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
