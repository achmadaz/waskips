<?php

// Register all styles
wp_register_style('bootstrap', get_template_directory_uri().'/libraries/bootstrap/css/bootstrap.min.css');
wp_register_style('font-awesome', get_template_directory_uri().'/libraries/font-awesome/css/font-awesome.min.css');
wp_register_style('owl-carousel', get_template_directory_uri().'/libraries/owl-carousel/dist/assets/owl.carousel.min.css');
wp_register_style('owl-theme', get_template_directory_uri().'/libraries/owl-carousel/dist/assets/owl.theme.default.min.css');
wp_register_style('custom-style', get_template_directory_uri().'/assets/css/custom-style.css');
wp_register_style('sf-menu', get_template_directory_uri().'/assets/css/superfish.css');


// Register all script
wp_register_script('bootstrap', get_template_directory_uri().'/libraries/bootstrap/js/bootstrap.min.js', array('jquery'), '3.3.7');
wp_register_script('owl-carousel', get_template_directory_uri().'/libraries/owl-carousel/dist/owl.carousel.min.js', array('jquery'), '2.2.1');
wp_register_script('sf-menu', get_template_directory_uri().'/assets/js/superfish.js', array('jquery'));
