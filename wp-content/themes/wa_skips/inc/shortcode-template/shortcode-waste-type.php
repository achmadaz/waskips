<?php global $opt_settings; ?>
	<div class="bg-pastel mt-30" style="padding: 25px 15px;">
		<div class="row">
		  	<?php for($i=1; $i<9; $i++): $waste_icon = $opt_settings['opt-waste-icon-'.$i]['url']; $waste_title = $opt_settings['opt-waste-title-'.$i]; $waste_list = $opt_settings['opt-waste-list-'.$i]; ?>
		    <div class="col-sm-3">
		      <div class="info-box">
		        <span><img src="<?= $waste_icon; ?>" alt=""></span>
			    <h2 class=""><?= $waste_title; ?></h2>
			    <ul class="waste-list">
			    	<?php foreach($waste_list as $list): ?>
			    	<li><span style="color: #9dc93a; font-size: 18px" class="fa fa-check"></span> <?= $list; ?></li>
			    	<?php endforeach ?>
			    </ul>
		      </div>
		    </div>
		    <?php if($i%4 == 0){
		    	echo '</div><div class="row">';
		    }
		    endfor; ?>
		</div>
	</div>
	