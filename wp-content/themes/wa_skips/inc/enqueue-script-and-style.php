<?php


function wa_skips_master_theme_scripts(){
  //require get_template_directory().'/inc/register-styles-and-scripts.php'
  global $opt_settings;

  // Font awesome
  if ($opt_settings['opt-enable-font-awesome'] == true) {
    wp_enqueue_style('font-awesome', get_template_directory_uri().'/libraries/font-awesome/css/font-awesome.min.css');
  }

  // Bootstrap style and script
  if ($opt_settings['opt-enable-bootstrap'] == true) {
    wp_enqueue_style('bootstrap', get_template_directory_uri().'/libraries/bootstrap/css/bootstrap.min.css');
    wp_enqueue_script('bootstrap', get_template_directory_uri().'/libraries/bootstrap/js/bootstrap.min.js', array('jquery'), '3.3.7', true);
  }

  //Owl Carousel only on font page
  if ($opt_settings['opt-enable-owl-carousel'] == true) {
    # code...
    wp_enqueue_style('owl-carousel', get_template_directory_uri().'/libraries/owl-carousel/dist/assets/owl.carousel.min.css');
    wp_enqueue_style('owl-carousel-theme', get_template_directory_uri().'/libraries/owl-carousel/dist/assets/owl.theme.default.min.css');
    wp_enqueue_script('owl-carousel', get_template_directory_uri().'/libraries/owl-carousel/dist/owl.carousel.min.js', array('jquery'), '2.2.0', true);
  }

  //Custom js
  wp_enqueue_script('custom', get_template_directory_uri().'/js/custom.js', array('jquery'), '1.0.0', true);

  //Wow js
  wp_enqueue_script('wow', get_template_directory_uri().'/assets/js/wow.min.js', array('jquery'), '1.0.0', true);

  //Bootstrap notify
  wp_enqueue_script('bootstrap-notify', get_template_directory_uri().'/assets/js/bootstrap-notify.min.js', array('jquery'), '1.0.0', true);

  //Superfish menu js
  wp_enqueue_script('sf-menu', get_template_directory_uri().'/assets/js/superfish.js', array('jquery'), '1.0.0', true);

  //Custom css
  wp_enqueue_style('custom-style', get_template_directory_uri().'/assets/css/custom-style.css');

  //Superfish menu css
  wp_enqueue_style('sf-menu', get_template_directory_uri().'/assets/css/superfish.css');

  //Animate css
  wp_enqueue_style('animate', get_template_directory_uri().'/assets/css/animate.css');

  //Sticky
  wp_enqueue_script('sticky', get_template_directory_uri().'/assets/js/jquery.sticky.js', array('jquery'), '1.0.0', true);
}

add_action( 'admin_enqueue_scripts', 'load_admin_style' );
function load_admin_style() {
  wp_register_style( 'admin_css', get_template_directory_uri() . '/admin-style.css', false, '1.0.0' );
//OR
  wp_enqueue_style( 'admin_css', get_template_directory_uri() . '/admin-style.css', false, '1.0.0' );
 }
