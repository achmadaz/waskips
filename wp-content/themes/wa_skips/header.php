<?php
global $opt_settings;
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wa_skips
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wa_skips' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="top-header line">
			<div class="container">
				<div class="top-bar flex center-vertical on-right">
					<nav id="top-navigation" class="top-navigation flex center-vertical on-right">
						<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-top',
							'menu_id'        => 'top-menu',
							'menu_class' => 'sf-menu',
						) );
						?>
					</nav><!-- #site-navigation -->
					<div class="social flex center-vertical">
						<ul>
							<?php if ($opt_settings['opt-enable-facebook']): ?>
								<li><a target="_blank" href="<?= $opt_settings['opt-facebook-url']; ?>" class="fa fa-facebook-square"></a></li>
							<?php endif; ?>
							<?php if ($opt_settings['opt-enable-twitter']): ?>
								<li><a target="_blank" href="<?= $opt_settings['opt-twitter-url']; ?>" class="fa fa-twitter-square"></a></li>
							<?php endif; ?>
							<?php if ($opt_settings['opt-enable-google-plus']): ?>
								<li><a target="_blank" href="<?= $opt_settings['opt-google-plus-url']; ?>" class="fa fa-google-plus-square"></a></li>
							<?php endif; ?>
							<?php if ($opt_settings['opt-enable-instagram']): ?>
								<li><a target="_blank" href="<?= $opt_settings['opt-instagram-url']; ?>" class="fa fa-instagram"></a></li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="site-branding flex center-vertical center line">
			<div class="container">
				<div class="row flex center-vertical">
					<div class="col-sm-2 nopadding">
						<div class="header-logo flex center-vertical">
							<a href="<?= home_url(); ?>"><img src="<?php echo $opt_settings['opt-logo']['url'];?>" alt="Logo" class="img-responsive"></a>
						</div>
					</div>
					<div class="col-sm-10 flex center-vertical on-right">
						<nav id="site-navigation" class="main-menu">
							<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Mobile Menu', 'wa_skips' ); ?></button>
							<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'primary-menu',
								'menu_class' => 'sf-menu',
							) );
							?>
						</nav><!-- #site-navigation -->
						<div class="phone-wrap flex center-vertical on-right">
							<div class="phone-text">
								<span style="color: #9dc93a; font-weight: bold;">Call Us</span>
								<a href="tel:<?= str_replace(' ', '', do_shortcode ('[phone-number]')); ?>"><span style="color: #c21010;"><?= do_shortcode ("[phone-number]") ; ?></span></a>
							</div>
							<div class="phone-img">
								<img src="<?php echo $opt_settings['opt-call-image']['url'] ?>" alt="" class="img-responsive">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- .site-branding -->
	</header><!-- #masthead -->
	<?php if(is_front_page()):?>
	<div class="banner-home line">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<div class="owl-carousel owl-theme" id="home-slider">
				      	<?php
				        	$slider = CFS()->get('image_slider', $post->ID);
				        	if ($slider):
				      	?>
				      	<?php foreach ($slider as $image): 
				      		$src = wp_get_attachment_image_src($image['image'], array('full'))[0];
				      		?>
				      		
				        <div class="item">
				          <div class="item-slider-home" style="background-image: url('<?php echo $src ?>');"></div>
				        </div>
				      	<?php endforeach; ?>
				    	<?php endif; ?>
				    </div>
					<div class="tagline">
						<h2>Find The Best Prices Quickly and Easily</h2>
					</div>
				</div>
				<div class="col-sm-5">
					
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<?php if(!is_front_page()):?>
	<div class="banner-page line text-center">
      	<?php
        	$banner = CFS()->get('banner', $post->ID);
        	$default = get_template_directory_uri().'/assets/images/banner-page.jpg';
        	
      	?>
          <div class="banner-img" style="background-image: url('<?= $banner == '' ? $default : $banner; ?>');"></div>
	    <div class="banner-text">
	    	<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	    	<h2 class="sub-title"><?= CFS()->get('subtitle'); ?></h2>
	    </div>
	    <div class="layer"></div>
	</div>
	<div class="button-box consult">
		<div class="button-shadow">
			<a href="#" data-toggle="modal" data-target="#free-consultation-form" class="button-green" rel="nofollow"><i class="fa fa-file-text fa-fw" aria-hidden="true"></i> Free Instant Quote</a>
		</div>
	</div>
	<?php endif; ?>

	<div id="content" class="site-content">
