jQuery(document).ready(function(){
  var is_sticky = false;
  var menu = jQuery('#mobile-menu');
  var window_width = window.innerWidth;

  //Owl carousel

  jQuery('.owl-carousel').each(function(index, el){
    var id = el.id;
    var owlSetup = {
      loop: true,
      nav: true,
      responsive: {
        0: {
          items: 1
        }
      },
      autoplay: true,
      autoplaySpeed: 700,
      navSpeed: 700,
      dots: false,
      transitionStyle: 'fade',
      animateIn: 'fadeIn',
      animateOut: 'fadeOut',
      navText: ['<span class="fa fa-angle-left"></span>','<span class="fa fa-angle-right"></span>']
    }
    if (id == 'banner-page') {
      owlSetup = {
        loop: true,
        responsive: {
          0: {
            items: 1
          }
        },
        animateIn: 'flipInX',
      }
    }else if (id == 'home-slider') {
      owlSetup = {
        loop: true,
        responsive: {
          0: {
            items: 1
          }
        },
        autoplay: true,
        dots:false,
      }
    }

    jQuery(this).owlCarousel(owlSetup);
  });

  jQuery(window).on('scroll', function(e){
    var y = this.pageYOffset;
    if (y > 50) {
      if (is_sticky == false) {
        jQuery('.top-menu').addClass('sticky');
        jQuery('#header-menu').addClass('sticky');
        jQuery('.bottom-menu').css({width: 1150});
      }
      is_sticky = true;
    }else if (y == 0) {
      is_sticky = false;
      jQuery('.top-menu').removeClass('sticky');
      jQuery('#header-menu').removeClass('sticky');
    }

    if (window_width > 990) {
      stickyForm(y);
    }

  });

  function stickyForm(y){
    var offset = jQuery('#main').outerHeight();
    var sticky_point = offset - (offset * 0.1);
    var form = jQuery('.form-wrapper');

    if (y >= sticky_point) {
      form.unstick();
      form.css({position:'absolute', top:803});
    }else {
      var sticky = jQuery('.is_sticky');
      if (sticky.length < 1) {
        form.sticky({topSpacing: 65});
      }
    }
  }

  //wow animation
  new WOW().init();

  // Toggle mobile menu
  jQuery('button.btn-toggle').click(function(e){
    e.preventDefault();
    menu.addClass('toggle-menu');
  })
  jQuery('#mobileClose').click(function(e){
    e.preventDefault();
    menu.removeClass('toggle-menu');
  })

  //wpcf7 contification
  jQuery('.wpcf7').on('wpcf7mailsent', function(e){
    var message = e.detail.apiResponse.message;
    jQuery.notify({
      title: '<strong>Notification</strong>',
      icon: 'fa fa-exclamation-circle',
      message: '<p>'+message+'</p>'
    },{
      type: 'success',
      placement: {
        from: 'top',
        align: 'center'
      },
      offset: {
        y: 20
      },
      z_index: 1055
    });
  });

  //currentDate();
  function currentDate(){
    date = new Date();
    current = date.getFullYear()+'-'+date.getMonth()+'-'+date.getDay();
    date_field = jQuery('#date');
    if (date_field) {
      date_field.attr('min', current);
    }
  }

  formSticky();
  function formSticky(){
    var form = jQuery('.form-wrapper');
    if (form && window_width > 990) {
      form.sticky({topSpacing: 65});
    }
  }

  //menu drop down
  jQuery('#primary-menu > li').on('click', function(e){
    var submenu = jQuery('#primary-menu > li > .sub-menu');
    if (submenu.length != 0 && window_width < 980) {
      e.preventDefault();
      submenu.slideToggle(250);
    }
  });

  searchToggle();
  function searchToggle(){
    var btn = jQuery('#searchIcon');
    var btn_close = jQuery('#searchForm').find('.close');
    var search_form = jQuery('#searchForm');
    btn.on('click', function(e){
      e.preventDefault();
      search_form.slideDown(300);
    });
    btn_close.on('click', function(e){
      e.preventDefault();
      search_form.slideUp(300);
    })
  }
});
