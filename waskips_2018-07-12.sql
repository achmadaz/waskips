# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.5.42)
# Database: waskips
# Generation Time: 2018-07-12 02:33:54 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table ws_cfs_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ws_cfs_sessions`;

CREATE TABLE `ws_cfs_sessions` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `data` text,
  `expires` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ws_cfs_sessions` WRITE;
/*!40000 ALTER TABLE `ws_cfs_sessions` DISABLE KEYS */;

INSERT INTO `ws_cfs_sessions` (`id`, `data`, `expires`)
VALUES
	('24a81a8d19842c30451ac3edd2d5a510','a:7:{s:7:\"post_id\";i:51;s:9:\"post_type\";s:4:\"post\";s:11:\"post_status\";s:5:\"draft\";s:12:\"field_groups\";a:3:{i:0;i:12;i:1;i:13;i:2;i:14;}s:20:\"confirmation_message\";s:0:\"\";s:16:\"confirmation_url\";s:0:\"\";s:9:\"front_end\";b:0;}','1531219430'),
	('816e3d4b939f572dd7258cb2768ed0ee','a:7:{s:7:\"post_id\";i:51;s:9:\"post_type\";s:4:\"post\";s:11:\"post_status\";s:5:\"draft\";s:12:\"field_groups\";a:3:{i:0;i:12;i:1;i:13;i:2;i:14;}s:20:\"confirmation_message\";s:0:\"\";s:16:\"confirmation_url\";s:0:\"\";s:9:\"front_end\";b:0;}','1531220487'),
	('c942e7b983ec3f9af4d64ebc335c9369','a:7:{s:7:\"post_id\";i:51;s:9:\"post_type\";s:4:\"post\";s:11:\"post_status\";s:5:\"draft\";s:12:\"field_groups\";a:3:{i:0;i:12;i:1;i:13;i:2;i:14;}s:20:\"confirmation_message\";s:0:\"\";s:16:\"confirmation_url\";s:0:\"\";s:9:\"front_end\";b:0;}','1531217893'),
	('ce1245599254fa570161e52aa6b976dd','a:7:{s:7:\"post_id\";i:51;s:9:\"post_type\";s:4:\"post\";s:11:\"post_status\";s:5:\"draft\";s:12:\"field_groups\";a:3:{i:0;i:12;i:1;i:13;i:2;i:14;}s:20:\"confirmation_message\";s:0:\"\";s:16:\"confirmation_url\";s:0:\"\";s:9:\"front_end\";b:0;}','1531219862');

/*!40000 ALTER TABLE `ws_cfs_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ws_cfs_values
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ws_cfs_values`;

CREATE TABLE `ws_cfs_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned DEFAULT NULL,
  `meta_id` int(10) unsigned DEFAULT NULL,
  `post_id` int(10) unsigned DEFAULT NULL,
  `base_field_id` int(10) unsigned DEFAULT '0',
  `hierarchy` text,
  `depth` int(10) unsigned DEFAULT '0',
  `weight` int(10) unsigned DEFAULT '0',
  `sub_weight` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `field_id_idx` (`field_id`),
  KEY `post_id_idx` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ws_cfs_values` WRITE;
/*!40000 ALTER TABLE `ws_cfs_values` DISABLE KEYS */;

INSERT INTO `ws_cfs_values` (`id`, `field_id`, `meta_id`, `post_id`, `base_field_id`, `hierarchy`, `depth`, `weight`, `sub_weight`)
VALUES
	(50,2,162,6,1,'1:0:2',1,0,0),
	(51,3,163,6,1,'1:0:3',1,0,0),
	(52,2,164,6,1,'1:1:2',1,1,0),
	(53,3,165,6,1,'1:1:3',1,1,0),
	(54,4,166,6,0,'',0,0,0),
	(55,5,167,6,0,'',0,0,0),
	(56,12,168,6,0,'',0,0,0),
	(57,6,169,6,0,'',0,0,0),
	(58,4,174,2,0,'',0,0,0),
	(59,5,175,2,0,'',0,0,0),
	(60,12,176,2,0,'',0,0,0),
	(61,6,177,2,0,'',0,0,0),
	(62,4,179,41,0,'',0,0,0),
	(63,5,180,41,0,'',0,0,0),
	(64,12,181,41,0,'',0,0,0),
	(65,6,182,41,0,'',0,0,0),
	(66,4,185,43,0,'',0,0,0),
	(67,5,186,43,0,'',0,0,0),
	(68,12,187,43,0,'',0,0,0),
	(69,6,188,43,0,'',0,0,0),
	(70,4,191,45,0,'',0,0,0),
	(71,5,192,45,0,'',0,0,0),
	(72,12,193,45,0,'',0,0,0),
	(73,6,194,45,0,'',0,0,0),
	(74,4,197,47,0,'',0,0,0),
	(75,5,198,47,0,'',0,0,0),
	(76,12,199,47,0,'',0,0,0),
	(77,6,200,47,0,'',0,0,0),
	(78,4,203,49,0,'',0,0,0),
	(79,5,204,49,0,'',0,0,0),
	(80,12,205,49,0,'',0,0,0),
	(81,6,206,49,0,'',0,0,0),
	(86,4,215,53,0,'',0,0,0),
	(87,5,216,53,0,'',0,0,0),
	(88,12,217,53,0,'',0,0,0),
	(89,6,218,53,0,'',0,0,0),
	(90,4,221,55,0,'',0,0,0),
	(91,5,222,55,0,'',0,0,0),
	(92,12,223,55,0,'',0,0,0),
	(93,6,224,55,0,'',0,0,0),
	(94,4,227,57,0,'',0,0,0),
	(95,5,228,57,0,'',0,0,0),
	(96,12,229,57,0,'',0,0,0),
	(97,6,230,57,0,'',0,0,0),
	(98,4,233,59,0,'',0,0,0),
	(99,5,234,59,0,'',0,0,0),
	(100,12,235,59,0,'',0,0,0),
	(101,6,236,59,0,'',0,0,0),
	(102,4,239,61,0,'',0,0,0),
	(103,5,240,61,0,'',0,0,0),
	(104,12,241,61,0,'',0,0,0),
	(105,6,242,61,0,'',0,0,0),
	(116,2,447,51,1,'1:0:2',1,0,0),
	(117,3,448,51,1,'1:0:3',1,0,0),
	(118,4,449,51,0,'',0,0,0),
	(119,5,450,51,0,'',0,0,0),
	(120,12,451,51,0,'',0,0,0),
	(121,6,452,51,0,'',0,0,0);

/*!40000 ALTER TABLE `ws_cfs_values` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ws_commentmeta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ws_commentmeta`;

CREATE TABLE `ws_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table ws_comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ws_comments`;

CREATE TABLE `ws_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `ws_comments` WRITE;
/*!40000 ALTER TABLE `ws_comments` DISABLE KEYS */;

INSERT INTO `ws_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`)
VALUES
	(1,1,'A WordPress Commenter','wapuu@wordpress.example','https://wordpress.org/','','2018-07-03 07:37:37','2018-07-03 07:37:37','Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.',0,'1','','',0,0);

/*!40000 ALTER TABLE `ws_comments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ws_links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ws_links`;

CREATE TABLE `ws_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table ws_options
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ws_options`;

CREATE TABLE `ws_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `ws_options` WRITE;
/*!40000 ALTER TABLE `ws_options` DISABLE KEYS */;

INSERT INTO `ws_options` (`option_id`, `option_name`, `option_value`, `autoload`)
VALUES
	(1,'siteurl','http://waskips:8888','yes'),
	(2,'home','http://waskips:8888','yes'),
	(3,'blogname','WA Skips','yes'),
	(4,'blogdescription','Just another WordPress site','yes'),
	(5,'users_can_register','0','yes'),
	(6,'admin_email','achmad.bcodes@gmail.com','yes'),
	(7,'start_of_week','1','yes'),
	(8,'use_balanceTags','0','yes'),
	(9,'use_smilies','1','yes'),
	(10,'require_name_email','1','yes'),
	(11,'comments_notify','1','yes'),
	(12,'posts_per_rss','10','yes'),
	(13,'rss_use_excerpt','0','yes'),
	(14,'mailserver_url','mail.example.com','yes'),
	(15,'mailserver_login','login@example.com','yes'),
	(16,'mailserver_pass','password','yes'),
	(17,'mailserver_port','110','yes'),
	(18,'default_category','1','yes'),
	(19,'default_comment_status','open','yes'),
	(20,'default_ping_status','open','yes'),
	(21,'default_pingback_flag','0','yes'),
	(22,'posts_per_page','10','yes'),
	(23,'date_format','F j, Y','yes'),
	(24,'time_format','g:i a','yes'),
	(25,'links_updated_date_format','F j, Y g:i a','yes'),
	(26,'comment_moderation','0','yes'),
	(27,'moderation_notify','1','yes'),
	(28,'permalink_structure','/%postname%/','yes'),
	(29,'rewrite_rules','a:105:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:31:\"cfs/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:41:\"cfs/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:61:\"cfs/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\"cfs/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\"cfs/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:37:\"cfs/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:20:\"cfs/([^/]+)/embed/?$\";s:51:\"index.php?post_type=cfs&name=$matches[1]&embed=true\";s:24:\"cfs/([^/]+)/trackback/?$\";s:45:\"index.php?post_type=cfs&name=$matches[1]&tb=1\";s:32:\"cfs/([^/]+)/page/?([0-9]{1,})/?$\";s:58:\"index.php?post_type=cfs&name=$matches[1]&paged=$matches[2]\";s:39:\"cfs/([^/]+)/comment-page-([0-9]{1,})/?$\";s:58:\"index.php?post_type=cfs&name=$matches[1]&cpage=$matches[2]\";s:28:\"cfs/([^/]+)(?:/([0-9]+))?/?$\";s:57:\"index.php?post_type=cfs&name=$matches[1]&page=$matches[2]\";s:20:\"cfs/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:30:\"cfs/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:50:\"cfs/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:45:\"cfs/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:45:\"cfs/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:26:\"cfs/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=6&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}','yes'),
	(30,'hack_file','0','yes'),
	(31,'blog_charset','UTF-8','yes'),
	(32,'moderation_keys','','no'),
	(33,'active_plugins','a:6:{i:0;s:35:\"redux-framework/redux-framework.php\";i:1;s:30:\"advanced-custom-fields/acf.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:26:\"custom-field-suite/cfs.php\";i:4;s:43:\"custom-post-type-ui/custom-post-type-ui.php\";i:5;s:23:\"wp-smushit/wp-smush.php\";}','yes'),
	(34,'category_base','','yes'),
	(35,'ping_sites','http://rpc.pingomatic.com/','yes'),
	(36,'comment_max_links','2','yes'),
	(37,'gmt_offset','0','yes'),
	(38,'default_email_category','1','yes'),
	(39,'recently_edited','','no'),
	(40,'template','wa_skips','yes'),
	(41,'stylesheet','wa_skips','yes'),
	(42,'comment_whitelist','1','yes'),
	(43,'blacklist_keys','','no'),
	(44,'comment_registration','0','yes'),
	(45,'html_type','text/html','yes'),
	(46,'use_trackback','0','yes'),
	(47,'default_role','subscriber','yes'),
	(48,'db_version','38590','yes'),
	(49,'uploads_use_yearmonth_folders','1','yes'),
	(50,'upload_path','','yes'),
	(51,'blog_public','0','yes'),
	(52,'default_link_category','2','yes'),
	(53,'show_on_front','page','yes'),
	(54,'tag_base','','yes'),
	(55,'show_avatars','1','yes'),
	(56,'avatar_rating','G','yes'),
	(57,'upload_url_path','','yes'),
	(58,'thumbnail_size_w','150','yes'),
	(59,'thumbnail_size_h','150','yes'),
	(60,'thumbnail_crop','1','yes'),
	(61,'medium_size_w','300','yes'),
	(62,'medium_size_h','300','yes'),
	(63,'avatar_default','mystery','yes'),
	(64,'large_size_w','1024','yes'),
	(65,'large_size_h','1024','yes'),
	(66,'image_default_link_type','none','yes'),
	(67,'image_default_size','','yes'),
	(68,'image_default_align','','yes'),
	(69,'close_comments_for_old_posts','0','yes'),
	(70,'close_comments_days_old','14','yes'),
	(71,'thread_comments','1','yes'),
	(72,'thread_comments_depth','5','yes'),
	(73,'page_comments','0','yes'),
	(74,'comments_per_page','50','yes'),
	(75,'default_comments_page','newest','yes'),
	(76,'comment_order','asc','yes'),
	(77,'sticky_posts','a:0:{}','yes'),
	(78,'widget_categories','a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes'),
	(79,'widget_text','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(80,'widget_rss','a:0:{}','yes'),
	(81,'uninstall_plugins','a:0:{}','no'),
	(82,'timezone_string','','yes'),
	(83,'page_for_posts','0','yes'),
	(84,'page_on_front','6','yes'),
	(85,'default_post_format','0','yes'),
	(86,'link_manager_enabled','0','yes'),
	(87,'finished_splitting_shared_terms','1','yes'),
	(88,'site_icon','0','yes'),
	(89,'medium_large_size_w','768','yes'),
	(90,'medium_large_size_h','0','yes'),
	(91,'wp_page_for_privacy_policy','3','yes'),
	(92,'initial_db_version','38590','yes'),
	(93,'ws_user_roles','a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}','yes'),
	(94,'fresh_site','0','yes'),
	(95,'widget_search','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes'),
	(96,'widget_recent-posts','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes'),
	(97,'widget_recent-comments','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes'),
	(98,'widget_archives','a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes'),
	(99,'widget_meta','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes'),
	(100,'sidebars_widgets','a:6:{s:19:\"wp_inactive_widgets\";a:0:{}s:8:\"footer-1\";a:1:{i:0;s:10:\"nav_menu-2\";}s:8:\"footer-2\";a:1:{i:0;s:10:\"nav_menu-3\";}s:8:\"footer-3\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}','yes'),
	(101,'widget_pages','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(102,'widget_calendar','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(103,'widget_media_audio','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(104,'widget_media_image','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(105,'widget_media_gallery','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(106,'widget_media_video','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(107,'widget_tag_cloud','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(108,'widget_nav_menu','a:3:{i:2;a:2:{s:5:\"title\";s:4:\"Menu\";s:8:\"nav_menu\";i:3;}i:3;a:2:{s:5:\"title\";s:13:\"Skip Bin Info\";s:8:\"nav_menu\";i:4;}s:12:\"_multiwidget\";i:1;}','yes'),
	(109,'widget_custom_html','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(110,'cron','a:5:{i:1531237058;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1531251458;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1531269507;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1531271750;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}','yes'),
	(111,'theme_mods_twentyseventeen','a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1530665111;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}','yes'),
	(120,'_site_transient_update_themes','O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1531208300;s:7:\"checked\";a:5:{s:13:\"twentyfifteen\";s:3:\"2.0\";s:15:\"twentyseventeen\";s:3:\"1.6\";s:13:\"twentysixteen\";s:3:\"1.5\";s:8:\"wa_skips\";s:5:\"1.0.0\";s:14:\"wa_skips_jabon\";s:5:\"1.0.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}','no'),
	(122,'_site_transient_timeout_browser_b9dc0782182e946806f5622d616a365c','1531208276','no'),
	(123,'_site_transient_browser_b9dc0782182e946806f5622d616a365c','a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"67.0.3396.99\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}','no'),
	(124,'can_compress_scripts','1','no'),
	(160,'current_theme','jabon_tour','yes'),
	(161,'theme_mods_wa_skips','a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:8:\"menu-top\";i:2;s:6:\"menu-1\";i:5;}s:18:\"custom_css_post_id\";i:-1;}','yes'),
	(162,'theme_switched','','yes'),
	(163,'widget_contactwidget','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(164,'widget_tourwidget','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(165,'widget_subscribewidget','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(168,'recently_activated','a:0:{}','yes'),
	(179,'wpcf7','a:2:{s:7:\"version\";s:5:\"5.0.2\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1530665567;s:7:\"version\";s:5:\"5.0.2\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}','yes'),
	(183,'wp-smush-last_settings','s:187:\"a:10:{s:11:\"networkwide\";b:0;s:4:\"auto\";i:1;s:5:\"lossy\";b:0;s:8:\"original\";b:0;s:9:\"keep_exif\";b:0;s:6:\"resize\";b:0;s:6:\"backup\";b:0;s:10:\"png_to_jpg\";b:0;s:7:\"nextgen\";b:0;s:2:\"s3\";b:0;}\";','no'),
	(184,'wdev-frash','a:3:{s:7:\"plugins\";a:1:{s:23:\"wp-smushit/wp-smush.php\";i:1530665567;}s:5:\"queue\";a:2:{s:32:\"7de3619981caadc55f30a002bfb299f6\";a:4:{s:6:\"plugin\";s:23:\"wp-smushit/wp-smush.php\";s:4:\"type\";s:5:\"email\";s:7:\"show_at\";i:1530665567;s:6:\"sticky\";b:1;}s:32:\"fc50097023d0d34c5a66f6cddcf77694\";a:3:{s:6:\"plugin\";s:23:\"wp-smushit/wp-smush.php\";s:4:\"type\";s:4:\"rate\";s:7:\"show_at\";i:1531270367;}}s:4:\"done\";a:0:{}}','no'),
	(185,'wp-smush-install-type','existing','no'),
	(186,'wp-smush-version','2.7.9.1','no'),
	(187,'wp-smush-skip-redirect','1','no'),
	(188,'redux_version_upgraded_from','3.6.9','yes'),
	(189,'cfs_next_field_id','13','yes'),
	(190,'cfs_version','2.5.12','yes'),
	(191,'acf_version','4.4.12','yes'),
	(193,'smush_option','a:1:{s:7:\"version\";s:7:\"2.7.9.1\";}','yes'),
	(194,'opt_settings','a:78:{s:8:\"last_tab\";s:1:\"2\";s:11:\"opt-favicon\";a:9:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";s:5:\"title\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:8:\"opt-logo\";a:9:{s:3:\"url\";s:64:\"http://waskips:8888/wp-content/uploads/2018/07/Wa-skips-logo.png\";s:2:\"id\";s:2:\"10\";s:6:\"height\";s:2:\"99\";s:5:\"width\";s:3:\"207\";s:9:\"thumbnail\";s:71:\"http://waskips:8888/wp-content/uploads/2018/07/Wa-skips-logo-150x99.png\";s:5:\"title\";s:13:\"Wa-skips-logo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:14:\"opt-call-image\";a:9:{s:3:\"url\";s:64:\"http://waskips:8888/wp-content/uploads/2018/07/call-button-2.png\";s:2:\"id\";s:2:\"11\";s:6:\"height\";s:3:\"113\";s:5:\"width\";s:3:\"136\";s:9:\"thumbnail\";s:64:\"http://waskips:8888/wp-content/uploads/2018/07/call-button-2.png\";s:5:\"title\";s:13:\"call-button-2\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:16:\"opt-payment-icon\";a:9:{s:3:\"url\";s:63:\"http://waskips:8888/wp-content/uploads/2018/07/payment-icon.png\";s:2:\"id\";s:2:\"39\";s:6:\"height\";s:2:\"31\";s:5:\"width\";s:3:\"104\";s:9:\"thumbnail\";s:63:\"http://waskips:8888/wp-content/uploads/2018/07/payment-icon.png\";s:5:\"title\";s:12:\"payment-icon\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:13:\"opt-copyright\";s:26:\"Copyright © 2018 WA Skips\";s:14:\"opt-home-title\";s:20:\"WELCOME TO WA SKIPS \";s:13:\"opt-home-desc\";s:309:\"The easiest and the cheapest way to book a skip bin\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\r\n\r\nThis is sample text\";s:15:\"opt-info-icon-1\";a:9:{s:3:\"url\";s:52:\"http://waskips:8888/wp-content/uploads/2018/07/a.png\";s:2:\"id\";s:2:\"26\";s:6:\"height\";s:3:\"159\";s:5:\"width\";s:3:\"159\";s:9:\"thumbnail\";s:60:\"http://waskips:8888/wp-content/uploads/2018/07/a-150x150.png\";s:5:\"title\";s:1:\"a\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:16:\"opt-info-title-1\";s:19:\"WIDE RANGE OF SKIPS\";s:15:\"opt-info-desc-1\";s:119:\"We offer a wide range of skip for different waste types. Whatever waste you need taken care of, we\'ve got bins to help.\";s:15:\"opt-info-icon-2\";a:9:{s:3:\"url\";s:52:\"http://waskips:8888/wp-content/uploads/2018/07/b.png\";s:2:\"id\";s:2:\"27\";s:6:\"height\";s:3:\"159\";s:5:\"width\";s:3:\"159\";s:9:\"thumbnail\";s:60:\"http://waskips:8888/wp-content/uploads/2018/07/b-150x150.png\";s:5:\"title\";s:1:\"b\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:16:\"opt-info-title-2\";s:18:\"WE HELP YOU CHOOSE\";s:15:\"opt-info-desc-2\";s:113:\"Simply start our simple 3 step process and we\'ll guide you through your options with helpful hints along the way.\";s:15:\"opt-info-icon-3\";a:9:{s:3:\"url\";s:52:\"http://waskips:8888/wp-content/uploads/2018/07/c.png\";s:2:\"id\";s:2:\"28\";s:6:\"height\";s:3:\"159\";s:5:\"width\";s:3:\"159\";s:9:\"thumbnail\";s:60:\"http://waskips:8888/wp-content/uploads/2018/07/c-150x150.png\";s:5:\"title\";s:1:\"c\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:16:\"opt-info-title-3\";s:27:\"SERVICING WESTERN AUSTRALIA\";s:15:\"opt-info-desc-3\";s:116:\"We cover Perth Western Australia area. Just input your suburb or post code and your need, we’ll come to serve you.\";s:12:\"opt-taglines\";a:6:{i:0;s:23:\"Lowest Prices Skip Bins\";i:1;s:19:\"Instant, Free Quote\";i:2;s:12:\"Free Booking\";i:3;s:28:\"Same Day Booking Before 10am\";i:4;s:28:\"7 Day Skip Bin Hire Standard\";i:5;s:20:\"Money Back Guarantee\";}s:16:\"opt-waste-icon-1\";a:9:{s:3:\"url\";s:52:\"http://waskips:8888/wp-content/uploads/2018/07/1.png\";s:2:\"id\";s:2:\"18\";s:6:\"height\";s:3:\"159\";s:5:\"width\";s:3:\"159\";s:9:\"thumbnail\";s:60:\"http://waskips:8888/wp-content/uploads/2018/07/1-150x150.png\";s:5:\"title\";s:1:\"1\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:17:\"opt-waste-title-1\";s:13:\"General Waste\";s:16:\"opt-waste-list-1\";a:5:{i:0;s:15:\"Household waste\";i:1;s:22:\"Light commercial waste\";i:2;s:33:\"Light building construction waste\";i:3;s:22:\"Furniture & appliances\";i:4;s:18:\"Light Green waste \";}s:16:\"opt-waste-icon-2\";a:9:{s:3:\"url\";s:52:\"http://waskips:8888/wp-content/uploads/2018/07/2.png\";s:2:\"id\";s:2:\"19\";s:6:\"height\";s:3:\"159\";s:5:\"width\";s:3:\"159\";s:9:\"thumbnail\";s:60:\"http://waskips:8888/wp-content/uploads/2018/07/2-150x150.png\";s:5:\"title\";s:1:\"2\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:17:\"opt-waste-title-2\";s:11:\"GREEN Waste\";s:16:\"opt-waste-list-2\";a:5:{i:0;s:17:\"Light green waste\";i:1;s:30:\"branches, leaves & palm fronds\";i:2;s:37:\"Woodchip & bark with NO soil attached\";i:3;s:63:\"Tree trunks smaller than 150mm in diameter and under 500mm long\";i:4;s:16:\"Untreated timber\";}s:16:\"opt-waste-icon-3\";a:9:{s:3:\"url\";s:52:\"http://waskips:8888/wp-content/uploads/2018/07/4.png\";s:2:\"id\";s:2:\"21\";s:6:\"height\";s:3:\"159\";s:5:\"width\";s:3:\"159\";s:9:\"thumbnail\";s:60:\"http://waskips:8888/wp-content/uploads/2018/07/4-150x150.png\";s:5:\"title\";s:1:\"4\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:17:\"opt-waste-title-3\";s:17:\"MIXED HEAVY WASTE\";s:16:\"opt-waste-list-3\";a:5:{i:0;s:71:\"Waste from domestic or commercial demolition, construction, renovation \";i:1;s:61:\"Treated timber like retaining walls, decking and floorboards \";i:2;s:14:\"Hardfill Waste\";i:3;s:16:\"Household waste \";i:4;s:11:\"Green waste\";}s:16:\"opt-waste-icon-4\";a:9:{s:3:\"url\";s:52:\"http://waskips:8888/wp-content/uploads/2018/07/3.png\";s:2:\"id\";s:2:\"20\";s:6:\"height\";s:3:\"159\";s:5:\"width\";s:3:\"159\";s:9:\"thumbnail\";s:60:\"http://waskips:8888/wp-content/uploads/2018/07/3-150x150.png\";s:5:\"title\";s:1:\"3\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:17:\"opt-waste-title-4\";s:11:\"Clean Waste\";s:16:\"opt-waste-list-4\";a:5:{i:0;s:34:\"Bricks and/or Bricks and mortar or\";i:1;s:36:\"Concrete (no bigger than 600x600) or\";i:2;s:20:\"Pebbles and rocks or\";i:3;s:13:\"Roof tiles or\";i:4;s:12:\"Floor tiles.\";}s:16:\"opt-waste-icon-5\";a:9:{s:3:\"url\";s:52:\"http://waskips:8888/wp-content/uploads/2018/07/5.png\";s:2:\"id\";s:2:\"22\";s:6:\"height\";s:3:\"159\";s:5:\"width\";s:3:\"159\";s:9:\"thumbnail\";s:60:\"http://waskips:8888/wp-content/uploads/2018/07/5-150x150.png\";s:5:\"title\";s:1:\"5\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:17:\"opt-waste-title-5\";s:12:\"MIXED RUBBLE\";s:16:\"opt-waste-list-5\";a:5:{i:0;s:28:\"Bricks, mortar and sandstone\";i:1;s:17:\"Pebbles and rocks\";i:2;s:21:\"Roofs and floor tiles\";i:3;s:40:\"Concrete rubble (no bigger than 600x600)\";i:4;s:67:\"N.B. No mixed materials of any sort except what is specified above.\";}s:16:\"opt-waste-icon-6\";a:9:{s:3:\"url\";s:52:\"http://waskips:8888/wp-content/uploads/2018/07/6.png\";s:2:\"id\";s:2:\"23\";s:6:\"height\";s:3:\"159\";s:5:\"width\";s:3:\"159\";s:9:\"thumbnail\";s:60:\"http://waskips:8888/wp-content/uploads/2018/07/6-150x150.png\";s:5:\"title\";s:1:\"6\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:17:\"opt-waste-title-6\";s:9:\"HOUSEHOLD\";s:16:\"opt-waste-list-6\";a:4:{i:0;s:15:\"Household waste\";i:1;s:12:\"Office waste\";i:2;s:24:\"Furniture and appliances\";i:3;s:18:\"Light green waste \";}s:16:\"opt-waste-icon-7\";a:9:{s:3:\"url\";s:52:\"http://waskips:8888/wp-content/uploads/2018/07/8.png\";s:2:\"id\";s:2:\"25\";s:6:\"height\";s:3:\"159\";s:5:\"width\";s:3:\"159\";s:9:\"thumbnail\";s:60:\"http://waskips:8888/wp-content/uploads/2018/07/8-150x150.png\";s:5:\"title\";s:1:\"8\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:17:\"opt-waste-title-7\";s:8:\"ASBESTOS\";s:16:\"opt-waste-list-7\";a:3:{i:0;s:38:\"Asbestos materials i.e. fibro sheeting\";i:1;s:27:\"Asbestos contaminated waste\";i:2;s:105:\"All skip bins hired for asbestos removal must be lined withdouble PVC orange or black builder’s plastic\";}s:16:\"opt-waste-icon-8\";a:9:{s:3:\"url\";s:52:\"http://waskips:8888/wp-content/uploads/2018/07/7.png\";s:2:\"id\";s:2:\"24\";s:6:\"height\";s:3:\"159\";s:5:\"width\";s:3:\"159\";s:9:\"thumbnail\";s:60:\"http://waskips:8888/wp-content/uploads/2018/07/7-150x150.png\";s:5:\"title\";s:1:\"7\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:17:\"opt-waste-title-8\";s:16:\"EXCAVATION WASTE\";s:16:\"opt-waste-list-8\";a:2:{i:0;s:54:\"Naturally occurring Soil/Dirt Rock, Sandstone and Clay\";i:1;s:33:\"Less than 10% Turf and Vegetation\";}s:16:\"opt-company-name\";s:8:\"WA Skips\";s:9:\"opt-email\";s:17:\"your@email.com.au\";s:9:\"opt-phone\";s:12:\"1234 567 890\";s:16:\"opt-working-hour\";s:0:\"\";s:11:\"opt-address\";s:32:\"Western Australia Australia 6065\";s:7:\"opt-map\";s:0:\"\";s:19:\"opt-enable-facebook\";s:1:\"1\";s:16:\"opt-facebook-url\";s:20:\"https://facebook.com\";s:22:\"opt-enable-tripadvisor\";s:0:\"\";s:19:\"opt-tripadvisor-url\";s:23:\"https://tripadvisor.com\";s:18:\"opt-enable-twitter\";s:1:\"1\";s:15:\"opt-twitter-url\";s:19:\"https://twitter.com\";s:22:\"opt-enable-google-plus\";s:1:\"1\";s:19:\"opt-google-plus-url\";s:23:\"https://plus.google.com\";s:14:\"opt-custom-css\";s:0:\"\";s:13:\"opt-head-code\";s:0:\"\";s:15:\"opt-footer-code\";s:0:\"\";s:20:\"opt-enable-bootstrap\";s:1:\"1\";s:23:\"opt-enable-font-awesome\";s:1:\"1\";s:20:\"opt-enable-lazy-load\";s:0:\"\";s:18:\"opt-lazy-load-blur\";s:1:\"1\";s:18:\"opt-enable-pace-js\";s:0:\"\";s:22:\"opt-enable-animate-css\";s:1:\"1\";s:23:\"opt-enable-owl-carousel\";s:1:\"1\";s:16:\"opt-owl-carousel\";s:0:\"\";s:26:\"opt-enable-js-match-height\";s:1:\"1\";s:28:\"opt-js-match-height-selector\";a:1:{i:0;s:0:\"\";}s:26:\"opt-location-title-content\";s:12:\"Our Location\";s:25:\"opt-location-desc-content\";s:0:\"\";s:27:\"opt-location-discover-title\";s:13:\"Discover Bali\";s:11:\"opt-loc-lat\";s:0:\"\";s:12:\"opt-loc-long\";s:0:\"\";s:13:\"opt-loc-title\";s:0:\"\";s:19:\"opt-loc-description\";s:0:\"\";s:12:\"opt-loc-link\";s:0:\"\";s:17:\"opt-location-icon\";a:9:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";s:5:\"title\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}','yes'),
	(195,'opt_settings-transients','a:2:{s:14:\"changed_values\";a:1:{s:13:\"opt-copyright\";s:0:\"\";}s:9:\"last_save\";i:1531116746;}','yes'),
	(196,'cptui_new_install','false','yes'),
	(213,'category_children','a:0:{}','yes'),
	(215,'nav_menu_options','a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}','yes'),
	(258,'_site_transient_update_core','O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.7.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.7.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.7-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.7-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.7\";s:7:\"version\";s:5:\"4.9.7\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1531208295;s:15:\"version_checked\";s:5:\"4.9.7\";s:12:\"translations\";a:0:{}}','no'),
	(260,'auto_core_update_notified','a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:23:\"achmad.bcodes@gmail.com\";s:7:\"version\";s:5:\"4.9.7\";s:9:\"timestamp\";i:1530835648;}','no'),
	(352,'_site_transient_timeout_theme_roots','1531210097','no'),
	(353,'_site_transient_theme_roots','a:5:{s:13:\"twentyfifteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";s:8:\"wa_skips\";s:7:\"/themes\";s:14:\"wa_skips_jabon\";s:7:\"/themes\";}','no'),
	(354,'_site_transient_update_plugins','O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1531208303;s:7:\"checked\";a:9:{s:30:\"advanced-custom-fields/acf.php\";s:6:\"4.4.12\";s:19:\"akismet/akismet.php\";s:5:\"4.0.3\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.0.2\";s:26:\"custom-field-suite/cfs.php\";s:6:\"2.5.12\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:5:\"1.5.8\";s:9:\"hello.php\";s:3:\"1.7\";s:35:\"redux-framework/redux-framework.php\";s:5:\"3.6.9\";s:23:\"wp-smushit/wp-smush.php\";s:7:\"2.7.9.1\";s:24:\"wordpress-seo/wp-seo.php\";s:5:\"7.7.3\";}s:8:\"response\";a:1:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.0.8\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.0.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.9.6\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:8:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:6:\"4.4.12\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.4.4.12.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.0.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.0.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:26:\"custom-field-suite/cfs.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/custom-field-suite\";s:4:\"slug\";s:18:\"custom-field-suite\";s:6:\"plugin\";s:26:\"custom-field-suite/cfs.php\";s:11:\"new_version\";s:6:\"2.5.12\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/custom-field-suite/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/custom-field-suite.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/custom-field-suite/assets/icon-256x256.png?rev=1112866\";s:2:\"1x\";s:71:\"https://ps.w.org/custom-field-suite/assets/icon-128x128.png?rev=1112866\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:43:\"custom-post-type-ui/custom-post-type-ui.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:33:\"w.org/plugins/custom-post-type-ui\";s:4:\"slug\";s:19:\"custom-post-type-ui\";s:6:\"plugin\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:11:\"new_version\";s:5:\"1.5.8\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/custom-post-type-ui/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/custom-post-type-ui.1.5.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-256x256.png?rev=1069557\";s:2:\"1x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-128x128.png?rev=1069557\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/custom-post-type-ui/assets/banner-1544x500.png?rev=1069557\";s:2:\"1x\";s:74:\"https://ps.w.org/custom-post-type-ui/assets/banner-772x250.png?rev=1069557\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:5:\"3.6.9\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.9.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554\";s:2:\"1x\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"wp-smushit/wp-smush.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:24:\"w.org/plugins/wp-smushit\";s:4:\"slug\";s:10:\"wp-smushit\";s:6:\"plugin\";s:23:\"wp-smushit/wp-smush.php\";s:11:\"new_version\";s:7:\"2.7.9.1\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/wp-smushit/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wp-smushit.2.7.9.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/wp-smushit/assets/icon-256x256.jpg?rev=1513049\";s:2:\"1x\";s:63:\"https://ps.w.org/wp-smushit/assets/icon-128x128.jpg?rev=1513049\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/wp-smushit/assets/banner-1544x500.png?rev=1863697\";s:2:\"1x\";s:65:\"https://ps.w.org/wp-smushit/assets/banner-772x250.png?rev=1863697\";}s:11:\"banners_rtl\";a:0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:5:\"7.7.3\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/plugin/wordpress-seo.7.7.3.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1834347\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1859687\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1859687\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}}}}','no');

/*!40000 ALTER TABLE `ws_options` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ws_postmeta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ws_postmeta`;

CREATE TABLE `ws_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `ws_postmeta` WRITE;
/*!40000 ALTER TABLE `ws_postmeta` DISABLE KEYS */;

INSERT INTO `ws_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`)
VALUES
	(1,2,'_wp_page_template','default'),
	(2,3,'_wp_page_template','default'),
	(3,5,'_form','<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit \"Send\"]'),
	(4,5,'_mail','a:8:{s:7:\"subject\";s:25:\"WA Skips \"[your-subject]\"\";s:6:\"sender\";s:31:\"[your-name] <wordpress@waskips>\";s:4:\"body\";s:166:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on WA Skips (http://waskips:8888)\";s:9:\"recipient\";s:23:\"achmad.bcodes@gmail.com\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";i:0;s:13:\"exclude_blank\";i:0;}'),
	(5,5,'_mail_2','a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:25:\"WA Skips \"[your-subject]\"\";s:6:\"sender\";s:28:\"WA Skips <wordpress@waskips>\";s:4:\"body\";s:108:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on WA Skips (http://waskips:8888)\";s:9:\"recipient\";s:12:\"[your-email]\";s:18:\"additional_headers\";s:33:\"Reply-To: achmad.bcodes@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";i:0;s:13:\"exclude_blank\";i:0;}'),
	(6,5,'_messages','a:8:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";}'),
	(7,5,'_additional_settings',NULL),
	(8,5,'_locale','en_US'),
	(9,3,'_edit_lock','1530666803:1'),
	(10,6,'_edit_last','1'),
	(11,6,'_edit_lock','1531106002:1'),
	(12,6,'_wp_page_template','default'),
	(22,9,'_menu_item_type','post_type'),
	(23,9,'_menu_item_menu_item_parent','0'),
	(24,9,'_menu_item_object_id','2'),
	(25,9,'_menu_item_object','page'),
	(26,9,'_menu_item_target',''),
	(27,9,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(28,9,'_menu_item_xfn',''),
	(29,9,'_menu_item_url',''),
	(31,10,'_wp_attached_file','2018/07/Wa-skips-logo.png'),
	(32,10,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"207\";s:6:\"height\";s:2:\"99\";s:4:\"file\";s:25:\"2018/07/Wa-skips-logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"Wa-skips-logo-150x99.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:2:\"99\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(33,10,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:35.7596513075965134476064122281968593597412109375;s:5:\"bytes\";i:5743;s:11:\"size_before\";i:16060;s:10:\"size_after\";i:10317;s:4:\"time\";d:0.08000000000000000166533453693773481063544750213623046875;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:36.8900000000000005684341886080801486968994140625;s:5:\"bytes\";i:2657;s:11:\"size_before\";i:7202;s:10:\"size_after\";i:4545;s:4:\"time\";d:0.040000000000000000832667268468867405317723751068115234375;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:34.840000000000003410605131648480892181396484375;s:5:\"bytes\";i:3086;s:11:\"size_before\";i:8858;s:10:\"size_after\";i:5772;s:4:\"time\";d:0.040000000000000000832667268468867405317723751068115234375;}}}'),
	(34,11,'_wp_attached_file','2018/07/call-button-2.png'),
	(35,11,'_wp_attachment_metadata','a:4:{s:5:\"width\";s:3:\"136\";s:6:\"height\";s:3:\"113\";s:4:\"file\";s:25:\"2018/07/call-button-2.png\";s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(36,11,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:24.934022728496795906494298833422362804412841796875;s:5:\"bytes\";i:9259;s:11:\"size_before\";i:37134;s:10:\"size_after\";i:27875;s:4:\"time\";d:0.0200000000000000004163336342344337026588618755340576171875;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:1:{s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:24.92999999999999971578290569595992565155029296875;s:5:\"bytes\";i:9259;s:11:\"size_before\";i:37134;s:10:\"size_after\";i:27875;s:4:\"time\";d:0.0200000000000000004163336342344337026588618755340576171875;}}}'),
	(37,12,'cfs_fields','a:3:{i:0;a:8:{s:2:\"id\";i:1;s:4:\"name\";s:12:\"image_slider\";s:5:\"label\";s:12:\"Image Slider\";s:4:\"type\";s:4:\"loop\";s:5:\"notes\";s:0:\"\";s:9:\"parent_id\";i:0;s:6:\"weight\";i:0;s:7:\"options\";a:5:{s:11:\"row_display\";s:1:\"1\";s:9:\"row_label\";s:8:\"Loop Row\";s:12:\"button_label\";s:7:\"Add Row\";s:9:\"limit_min\";s:0:\"\";s:9:\"limit_max\";s:0:\"\";}}i:1;a:8:{s:2:\"id\";i:2;s:4:\"name\";s:5:\"image\";s:5:\"label\";s:5:\"Image\";s:4:\"type\";s:4:\"file\";s:5:\"notes\";s:0:\"\";s:9:\"parent_id\";i:1;s:6:\"weight\";i:1;s:7:\"options\";a:3:{s:9:\"file_type\";s:5:\"image\";s:12:\"return_value\";s:2:\"id\";s:8:\"required\";s:1:\"1\";}}i:2;a:8:{s:2:\"id\";i:3;s:4:\"name\";s:10:\"image_text\";s:5:\"label\";s:10:\"Image Text\";s:4:\"type\";s:4:\"text\";s:5:\"notes\";s:0:\"\";s:9:\"parent_id\";i:1;s:6:\"weight\";i:2;s:7:\"options\";a:2:{s:13:\"default_value\";s:0:\"\";s:8:\"required\";s:1:\"0\";}}}'),
	(38,12,'cfs_rules','a:1:{s:10:\"post_types\";a:2:{s:8:\"operator\";s:2:\"==\";s:6:\"values\";a:2:{i:0;s:4:\"page\";i:1;s:4:\"tour\";}}}'),
	(39,12,'cfs_extras','a:3:{s:5:\"order\";s:1:\"0\";s:7:\"context\";s:6:\"normal\";s:11:\"hide_editor\";s:1:\"0\";}'),
	(40,13,'cfs_fields','a:3:{i:0;a:8:{s:2:\"id\";s:1:\"4\";s:4:\"name\";s:5:\"title\";s:5:\"label\";s:5:\"Title\";s:4:\"type\";s:4:\"text\";s:5:\"notes\";s:0:\"\";s:9:\"parent_id\";i:0;s:6:\"weight\";i:0;s:7:\"options\";a:2:{s:13:\"default_value\";s:0:\"\";s:8:\"required\";s:1:\"0\";}}i:1;a:8:{s:2:\"id\";s:1:\"5\";s:4:\"name\";s:8:\"subtitle\";s:5:\"label\";s:8:\"Subtitle\";s:4:\"type\";s:4:\"text\";s:5:\"notes\";s:0:\"\";s:9:\"parent_id\";i:0;s:6:\"weight\";i:1;s:7:\"options\";a:2:{s:13:\"default_value\";s:0:\"\";s:8:\"required\";s:1:\"0\";}}i:2;a:8:{s:2:\"id\";s:2:\"12\";s:4:\"name\";s:10:\"waste_type\";s:5:\"label\";s:10:\"Waste Type\";s:4:\"type\";s:7:\"wysiwyg\";s:5:\"notes\";s:0:\"\";s:9:\"parent_id\";i:0;s:6:\"weight\";i:2;s:7:\"options\";a:2:{s:10:\"formatting\";s:7:\"default\";s:8:\"required\";s:1:\"0\";}}}'),
	(41,13,'cfs_rules','a:1:{s:10:\"post_types\";a:2:{s:8:\"operator\";s:2:\"==\";s:6:\"values\";a:1:{i:0;s:4:\"page\";}}}'),
	(42,13,'cfs_extras','a:3:{s:5:\"order\";s:1:\"0\";s:7:\"context\";s:6:\"normal\";s:11:\"hide_editor\";s:1:\"0\";}'),
	(43,14,'cfs_fields','a:1:{i:0;a:8:{s:2:\"id\";i:6;s:4:\"name\";s:5:\"image\";s:5:\"label\";s:5:\"Image\";s:4:\"type\";s:4:\"file\";s:5:\"notes\";s:0:\"\";s:9:\"parent_id\";i:0;s:6:\"weight\";i:0;s:7:\"options\";a:3:{s:9:\"file_type\";s:5:\"image\";s:12:\"return_value\";s:3:\"url\";s:8:\"required\";s:1:\"0\";}}}'),
	(44,14,'cfs_rules','a:1:{s:10:\"post_types\";a:2:{s:8:\"operator\";s:2:\"==\";s:6:\"values\";a:2:{i:0;s:4:\"page\";i:1;s:4:\"tour\";}}}'),
	(45,14,'cfs_extras','a:3:{s:5:\"order\";s:1:\"0\";s:7:\"context\";s:6:\"normal\";s:11:\"hide_editor\";s:1:\"0\";}'),
	(46,15,'cfs_fields','a:5:{i:0;a:8:{s:2:\"id\";i:7;s:4:\"name\";s:5:\"price\";s:5:\"label\";s:5:\"Price\";s:4:\"type\";s:4:\"text\";s:5:\"notes\";s:0:\"\";s:9:\"parent_id\";i:0;s:6:\"weight\";i:0;s:7:\"options\";a:2:{s:13:\"default_value\";s:0:\"\";s:8:\"required\";s:1:\"0\";}}i:1;a:8:{s:2:\"id\";i:8;s:4:\"name\";s:8:\"currency\";s:5:\"label\";s:8:\"Currency\";s:4:\"type\";s:4:\"text\";s:5:\"notes\";s:0:\"\";s:9:\"parent_id\";i:0;s:6:\"weight\";i:1;s:7:\"options\";a:2:{s:13:\"default_value\";s:0:\"\";s:8:\"required\";s:1:\"0\";}}i:2;a:8:{s:2:\"id\";i:9;s:4:\"name\";s:7:\"popular\";s:5:\"label\";s:7:\"Popular\";s:4:\"type\";s:10:\"true_false\";s:5:\"notes\";s:0:\"\";s:9:\"parent_id\";i:0;s:6:\"weight\";i:2;s:7:\"options\";a:2:{s:7:\"message\";s:0:\"\";s:8:\"required\";s:1:\"0\";}}i:3;a:8:{s:2:\"id\";i:10;s:4:\"name\";s:10:\"price_type\";s:5:\"label\";s:10:\"Price Type\";s:4:\"type\";s:6:\"select\";s:5:\"notes\";s:0:\"\";s:9:\"parent_id\";i:0;s:6:\"weight\";i:3;s:7:\"options\";a:4:{s:7:\"choices\";a:2:{s:6:\"/ Tour\";s:6:\"/ Tour\";s:5:\"/ Car\";s:5:\"/ Car\";}s:8:\"multiple\";s:1:\"0\";s:7:\"select2\";s:1:\"0\";s:8:\"required\";s:1:\"0\";}}i:4;a:8:{s:2:\"id\";i:11;s:4:\"name\";s:15:\"additional_info\";s:5:\"label\";s:22:\"Additional Information\";s:4:\"type\";s:4:\"text\";s:5:\"notes\";s:0:\"\";s:9:\"parent_id\";i:0;s:6:\"weight\";i:4;s:7:\"options\";a:2:{s:13:\"default_value\";s:0:\"\";s:8:\"required\";s:1:\"0\";}}}'),
	(47,15,'cfs_rules','a:1:{s:10:\"post_types\";a:2:{s:8:\"operator\";s:2:\"==\";s:6:\"values\";a:1:{i:0;s:4:\"tour\";}}}'),
	(48,15,'cfs_extras','a:3:{s:5:\"order\";s:1:\"0\";s:7:\"context\";s:6:\"normal\";s:11:\"hide_editor\";s:1:\"0\";}'),
	(62,17,'_wp_attached_file','2018/07/slider-1.jpg'),
	(63,17,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"659\";s:6:\"height\";s:3:\"500\";s:4:\"file\";s:20:\"2018/07/slider-1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"slider-1-150x150.jpg\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"slider-1-300x228.jpg\";s:5:\"width\";s:3:\"300\";s:6:\"height\";s:3:\"228\";s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(64,17,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:5.35187139601401096200561369187198579311370849609375;s:5:\"bytes\";i:3499;s:11:\"size_before\";i:65379;s:10:\"size_after\";i:61880;s:4:\"time\";d:0.059999999999999997779553950749686919152736663818359375;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:3:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.61000000000000031974423109204508364200592041015625;s:5:\"bytes\";i:354;s:11:\"size_before\";i:5356;s:10:\"size_after\";i:5002;s:4:\"time\";d:0.01000000000000000020816681711721685132943093776702880859375;}s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.660000000000000142108547152020037174224853515625;s:5:\"bytes\";i:765;s:11:\"size_before\";i:11495;s:10:\"size_after\";i:10730;s:4:\"time\";d:0.0200000000000000004163336342344337026588618755340576171875;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.9000000000000003552713678800500929355621337890625;s:5:\"bytes\";i:2380;s:11:\"size_before\";i:48528;s:10:\"size_after\";i:46148;s:4:\"time\";d:0.0299999999999999988897769753748434595763683319091796875;}}}'),
	(72,18,'_wp_attached_file','2018/07/1.png'),
	(73,18,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:13:\"2018/07/1.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"1-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(74,19,'_wp_attached_file','2018/07/2.png'),
	(75,19,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:13:\"2018/07/2.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"2-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(76,20,'_wp_attached_file','2018/07/3.png'),
	(77,20,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:13:\"2018/07/3.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"3-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(78,21,'_wp_attached_file','2018/07/4.png'),
	(79,21,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:13:\"2018/07/4.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"4-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(80,18,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:33.87194915827192431834191665984690189361572265625;s:5:\"bytes\";i:10020;s:11:\"size_before\";i:29582;s:10:\"size_after\";i:19562;s:4:\"time\";d:0.13000000000000000444089209850062616169452667236328125;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:33.42999999999999971578290569595992565155029296875;s:5:\"bytes\";i:7075;s:11:\"size_before\";i:21163;s:10:\"size_after\";i:14088;s:4:\"time\";d:0.0899999999999999966693309261245303787291049957275390625;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:34.97999999999999687361196265555918216705322265625;s:5:\"bytes\";i:2945;s:11:\"size_before\";i:8419;s:10:\"size_after\";i:5474;s:4:\"time\";d:0.040000000000000000832667268468867405317723751068115234375;}}}'),
	(81,19,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:30.7689897217347692048861063085496425628662109375;s:5:\"bytes\";i:9819;s:11:\"size_before\";i:31912;s:10:\"size_after\";i:22093;s:4:\"time\";d:0.1000000000000000055511151231257827021181583404541015625;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:30.8299999999999982946974341757595539093017578125;s:5:\"bytes\";i:6798;s:11:\"size_before\";i:22053;s:10:\"size_after\";i:15255;s:4:\"time\";d:0.059999999999999997779553950749686919152736663818359375;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:30.6400000000000005684341886080801486968994140625;s:5:\"bytes\";i:3021;s:11:\"size_before\";i:9859;s:10:\"size_after\";i:6838;s:4:\"time\";d:0.040000000000000000832667268468867405317723751068115234375;}}}'),
	(82,22,'_wp_attached_file','2018/07/5.png'),
	(83,22,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:13:\"2018/07/5.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"5-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(84,20,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:30.555270098996334837693211738951504230499267578125;s:5:\"bytes\";i:8920;s:11:\"size_before\";i:29193;s:10:\"size_after\";i:20273;s:4:\"time\";d:0.0899999999999999966693309261245303787291049957275390625;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:30.219999999999998863131622783839702606201171875;s:5:\"bytes\";i:6192;s:11:\"size_before\";i:20488;s:10:\"size_after\";i:14296;s:4:\"time\";d:0.05000000000000000277555756156289135105907917022705078125;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:31.339999999999999857891452847979962825775146484375;s:5:\"bytes\";i:2728;s:11:\"size_before\";i:8705;s:10:\"size_after\";i:5977;s:4:\"time\";d:0.040000000000000000832667268468867405317723751068115234375;}}}'),
	(85,23,'_wp_attached_file','2018/07/6.png'),
	(86,23,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:13:\"2018/07/6.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"6-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(87,24,'_wp_attached_file','2018/07/7.png'),
	(88,24,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:13:\"2018/07/7.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"7-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(89,22,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:32.53039613021309861551344511099159717559814453125;s:5:\"bytes\";i:9953;s:11:\"size_before\";i:30596;s:10:\"size_after\";i:20643;s:4:\"time\";d:0.13000000000000000444089209850062616169452667236328125;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:32.280000000000001136868377216160297393798828125;s:5:\"bytes\";i:6913;s:11:\"size_before\";i:21418;s:10:\"size_after\";i:14505;s:4:\"time\";d:0.1000000000000000055511151231257827021181583404541015625;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:33.11999999999999744204615126363933086395263671875;s:5:\"bytes\";i:3040;s:11:\"size_before\";i:9178;s:10:\"size_after\";i:6138;s:4:\"time\";d:0.0299999999999999988897769753748434595763683319091796875;}}}'),
	(90,21,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:31.765396256717526313195776310749351978302001953125;s:5:\"bytes\";i:10285;s:11:\"size_before\";i:32378;s:10:\"size_after\";i:22093;s:4:\"time\";d:0.1000000000000000055511151231257827021181583404541015625;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:30.75;s:5:\"bytes\";i:6857;s:11:\"size_before\";i:22300;s:10:\"size_after\";i:15443;s:4:\"time\";d:0.059999999999999997779553950749686919152736663818359375;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:34.00999999999999801048033987171947956085205078125;s:5:\"bytes\";i:3428;s:11:\"size_before\";i:10078;s:10:\"size_after\";i:6650;s:4:\"time\";d:0.040000000000000000832667268468867405317723751068115234375;}}}'),
	(91,25,'_wp_attached_file','2018/07/8.png'),
	(92,25,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:13:\"2018/07/8.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"8-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(93,23,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:34.3233302395930053307893103919923305511474609375;s:5:\"bytes\";i:9985;s:11:\"size_before\";i:29091;s:10:\"size_after\";i:19106;s:4:\"time\";d:0.13000000000000000444089209850062616169452667236328125;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:33.1099999999999994315658113919198513031005859375;s:5:\"bytes\";i:6851;s:11:\"size_before\";i:20691;s:10:\"size_after\";i:13840;s:4:\"time\";d:0.08000000000000000166533453693773481063544750213623046875;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:37.31000000000000227373675443232059478759765625;s:5:\"bytes\";i:3134;s:11:\"size_before\";i:8400;s:10:\"size_after\";i:5266;s:4:\"time\";d:0.05000000000000000277555756156289135105907917022705078125;}}}'),
	(94,26,'_wp_attached_file','2018/07/a.png'),
	(95,26,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:13:\"2018/07/a.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"a-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(96,27,'_wp_attached_file','2018/07/b.png'),
	(97,27,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:13:\"2018/07/b.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"b-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(98,24,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:32.1022920492416545812375261448323726654052734375;s:5:\"bytes\";i:9440;s:11:\"size_before\";i:29406;s:10:\"size_after\";i:19966;s:4:\"time\";d:0.1100000000000000144328993201270350255072116851806640625;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:31.57000000000000028421709430404007434844970703125;s:5:\"bytes\";i:6610;s:11:\"size_before\";i:20935;s:10:\"size_after\";i:14325;s:4:\"time\";d:0.070000000000000006661338147750939242541790008544921875;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:33.409999999999996589394868351519107818603515625;s:5:\"bytes\";i:2830;s:11:\"size_before\";i:8471;s:10:\"size_after\";i:5641;s:4:\"time\";d:0.040000000000000000832667268468867405317723751068115234375;}}}'),
	(99,25,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:29.941959713212700222584317089058458805084228515625;s:5:\"bytes\";i:11401;s:11:\"size_before\";i:38077;s:10:\"size_after\";i:26676;s:4:\"time\";d:0.1000000000000000055511151231257827021181583404541015625;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:29.5799999999999982946974341757595539093017578125;s:5:\"bytes\";i:7476;s:11:\"size_before\";i:25270;s:10:\"size_after\";i:17794;s:4:\"time\";d:0.059999999999999997779553950749686919152736663818359375;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:30.64999999999999857891452847979962825775146484375;s:5:\"bytes\";i:3925;s:11:\"size_before\";i:12807;s:10:\"size_after\";i:8882;s:4:\"time\";d:0.040000000000000000832667268468867405317723751068115234375;}}}'),
	(100,28,'_wp_attached_file','2018/07/c.png'),
	(101,28,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:13:\"2018/07/c.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"c-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(102,26,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:30.50567469179234336706940666772425174713134765625;s:5:\"bytes\";i:9972;s:11:\"size_before\";i:32689;s:10:\"size_after\";i:22717;s:4:\"time\";d:0.11000000000000000055511151231257827021181583404541015625;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:30.42999999999999971578290569595992565155029296875;s:5:\"bytes\";i:6884;s:11:\"size_before\";i:22625;s:10:\"size_after\";i:15741;s:4:\"time\";d:0.059999999999999997779553950749686919152736663818359375;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:30.67999999999999971578290569595992565155029296875;s:5:\"bytes\";i:3088;s:11:\"size_before\";i:10064;s:10:\"size_after\";i:6976;s:4:\"time\";d:0.05000000000000000277555756156289135105907917022705078125;}}}'),
	(103,29,'_wp_attached_file','2018/07/d-active.png'),
	(104,29,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:20:\"2018/07/d-active.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"d-active-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(105,30,'_wp_attached_file','2018/07/d.png'),
	(106,30,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:13:\"2018/07/d.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"d-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(107,27,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:30.113081679140908164526990731246769428253173828125;s:5:\"bytes\";i:7403;s:11:\"size_before\";i:24584;s:10:\"size_after\";i:17181;s:4:\"time\";d:0.11000000000000000055511151231257827021181583404541015625;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:29.8599999999999994315658113919198513031005859375;s:5:\"bytes\";i:5239;s:11:\"size_before\";i:17548;s:10:\"size_after\";i:12309;s:4:\"time\";d:0.059999999999999997779553950749686919152736663818359375;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:30.760000000000001563194018672220408916473388671875;s:5:\"bytes\";i:2164;s:11:\"size_before\";i:7036;s:10:\"size_after\";i:4872;s:4:\"time\";d:0.05000000000000000277555756156289135105907917022705078125;}}}'),
	(108,28,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:27.013084714796203655851059011183679103851318359375;s:5:\"bytes\";i:10054;s:11:\"size_before\";i:37219;s:10:\"size_after\";i:27165;s:4:\"time\";d:0.169999999999999984456877655247808434069156646728515625;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:25.030000000000001136868377216160297393798828125;s:5:\"bytes\";i:6085;s:11:\"size_before\";i:24306;s:10:\"size_after\";i:18221;s:4:\"time\";d:0.0899999999999999966693309261245303787291049957275390625;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:30.739999999999998436805981327779591083526611328125;s:5:\"bytes\";i:3969;s:11:\"size_before\";i:12913;s:10:\"size_after\";i:8944;s:4:\"time\";d:0.08000000000000000166533453693773481063544750213623046875;}}}'),
	(109,31,'_wp_attached_file','2018/07/e-active.png'),
	(110,31,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:20:\"2018/07/e-active.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"e-active-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(111,32,'_wp_attached_file','2018/07/e.png'),
	(112,32,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:13:\"2018/07/e.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"e-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(113,29,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:36.1604938271604936517178430221974849700927734375;s:5:\"bytes\";i:8787;s:11:\"size_before\";i:24300;s:10:\"size_after\";i:15513;s:4:\"time\";d:0.1000000000000000055511151231257827021181583404541015625;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:35.99000000000000198951966012828052043914794921875;s:5:\"bytes\";i:6463;s:11:\"size_before\";i:17957;s:10:\"size_after\";i:11494;s:4:\"time\";d:0.059999999999999997779553950749686919152736663818359375;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:36.6400000000000005684341886080801486968994140625;s:5:\"bytes\";i:2324;s:11:\"size_before\";i:6343;s:10:\"size_after\";i:4019;s:4:\"time\";d:0.040000000000000000832667268468867405317723751068115234375;}}}'),
	(114,30,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:30.562455642299500624403663096018135547637939453125;s:5:\"bytes\";i:3445;s:11:\"size_before\";i:11272;s:10:\"size_after\";i:7827;s:4:\"time\";d:0.0899999999999999966693309261245303787291049957275390625;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:25.059999999999998721023075631819665431976318359375;s:5:\"bytes\";i:1599;s:11:\"size_before\";i:6380;s:10:\"size_after\";i:4781;s:4:\"time\";d:0.05000000000000000277555756156289135105907917022705078125;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:37.74000000000000198951966012828052043914794921875;s:5:\"bytes\";i:1846;s:11:\"size_before\";i:4892;s:10:\"size_after\";i:3046;s:4:\"time\";d:0.040000000000000000832667268468867405317723751068115234375;}}}'),
	(115,33,'_wp_attached_file','2018/07/f-active.png'),
	(116,33,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:20:\"2018/07/f-active.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"f-active-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(117,31,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:35.7808169385251488847643486224114894866943359375;s:5:\"bytes\";i:7875;s:11:\"size_before\";i:22009;s:10:\"size_after\";i:14134;s:4:\"time\";d:0.1000000000000000055511151231257827021181583404541015625;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:35.9200000000000017053025658242404460906982421875;s:5:\"bytes\";i:5902;s:11:\"size_before\";i:16432;s:10:\"size_after\";i:10530;s:4:\"time\";d:0.059999999999999997779553950749686919152736663818359375;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:35.38000000000000255795384873636066913604736328125;s:5:\"bytes\";i:1973;s:11:\"size_before\";i:5577;s:10:\"size_after\";i:3604;s:4:\"time\";d:0.040000000000000000832667268468867405317723751068115234375;}}}'),
	(118,34,'_wp_attached_file','2018/07/f.png'),
	(119,34,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"159\";s:6:\"height\";s:3:\"159\";s:4:\"file\";s:13:\"2018/07/f.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"f-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(120,32,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:30.951694996638817514167385525070130825042724609375;s:5:\"bytes\";i:3223;s:11:\"size_before\";i:10413;s:10:\"size_after\";i:7190;s:4:\"time\";d:0.08000000000000000166533453693773481063544750213623046875;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:26.42999999999999971578290569595992565155029296875;s:5:\"bytes\";i:1602;s:11:\"size_before\";i:6061;s:10:\"size_after\";i:4459;s:4:\"time\";d:0.05000000000000000277555756156289135105907917022705078125;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:37.25;s:5:\"bytes\";i:1621;s:11:\"size_before\";i:4352;s:10:\"size_after\";i:2731;s:4:\"time\";d:0.0299999999999999988897769753748434595763683319091796875;}}}'),
	(122,36,'_wp_attached_file','2018/07/phone.png'),
	(123,36,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:3:\"182\";s:6:\"height\";s:3:\"186\";s:4:\"file\";s:17:\"2018/07/phone.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"phone-150x150.png\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(124,33,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:37.61832561570298594233463518321514129638671875;s:5:\"bytes\";i:9180;s:11:\"size_before\";i:24403;s:10:\"size_after\";i:15223;s:4:\"time\";d:0.0899999999999999966693309261245303787291049957275390625;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:37.61999999999999744204615126363933086395263671875;s:5:\"bytes\";i:6777;s:11:\"size_before\";i:18013;s:10:\"size_after\";i:11236;s:4:\"time\";d:0.059999999999999997779553950749686919152736663818359375;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:37.6099999999999994315658113919198513031005859375;s:5:\"bytes\";i:2403;s:11:\"size_before\";i:6390;s:10:\"size_after\";i:3987;s:4:\"time\";d:0.0299999999999999988897769753748434595763683319091796875;}}}'),
	(125,34,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:31.334359996376480950175391626544296741485595703125;s:5:\"bytes\";i:3459;s:11:\"size_before\";i:11039;s:10:\"size_after\";i:7580;s:4:\"time\";d:0.1100000000000000144328993201270350255072116851806640625;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:27.60000000000000142108547152020037174224853515625;s:5:\"bytes\";i:1728;s:11:\"size_before\";i:6260;s:10:\"size_after\";i:4532;s:4:\"time\";d:0.040000000000000000832667268468867405317723751068115234375;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:36.219999999999998863131622783839702606201171875;s:5:\"bytes\";i:1731;s:11:\"size_before\";i:4779;s:10:\"size_after\";i:3048;s:4:\"time\";d:0.070000000000000006661338147750939242541790008544921875;}}}'),
	(127,36,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:30.06535947712418277433243929408490657806396484375;s:5:\"bytes\";i:11638;s:11:\"size_before\";i:38709;s:10:\"size_after\";i:27071;s:4:\"time\";d:0.12000000000000000943689570931383059360086917877197265625;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:29.910000000000000142108547152020037174224853515625;s:5:\"bytes\";i:7208;s:11:\"size_before\";i:24100;s:10:\"size_after\";i:16892;s:4:\"time\";d:0.05000000000000000277555756156289135105907917022705078125;}s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:30.32000000000000028421709430404007434844970703125;s:5:\"bytes\";i:4430;s:11:\"size_before\";i:14609;s:10:\"size_after\";i:10179;s:4:\"time\";d:0.070000000000000006661338147750939242541790008544921875;}}}'),
	(128,13,'_edit_lock','1531109176:1'),
	(129,13,'_edit_last','1'),
	(162,6,'image','17'),
	(163,6,'image_text',''),
	(164,6,'image','17'),
	(165,6,'image_text',''),
	(166,6,'title',''),
	(167,6,'subtitle',''),
	(168,6,'waste_type','<h2>WASTE TYPES</h2><p>We made our waste type identification process easy and simple to follow, we hope it helps you make an informed choice of what is acceptable rubbish/waste to be placed inside each of our 8 waste type skip bins.</p><p>[waste-type]</p><p><br />It is important you know, that the skip bin suppliers and the waste facilities are strict for the correct rubbish/waste type to be placed in the bins and that you will incur additional charges for any incorrect or prohibited materials.</p><p><a href=\"http://waskips:8888/sample-page/\">[read-more]</a></p><h2>WHAT BIN SIZE SHOULD YOU HIRE?</h2><p>A wide variety of skip bins are available.It’s important to choose the right size to suit your needs to ensure you have a bin that is big enough, or, in turn, don’t spend extra money by booking a skip bin that is too big for your requirements.</p><p><a href=\"http://waskips:8888/sample-page/\">[read-more]</a></p>'),
	(169,6,'image',''),
	(170,39,'_wp_attached_file','2018/07/payment-icon.png'),
	(171,39,'_wp_attachment_metadata','a:4:{s:5:\"width\";s:3:\"104\";s:6:\"height\";s:2:\"31\";s:4:\"file\";s:24:\"2018/07/payment-icon.png\";s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(172,39,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:22.705314009661837104658843600191175937652587890625;s:5:\"bytes\";i:1175;s:11:\"size_before\";i:5175;s:10:\"size_after\";i:4000;s:4:\"time\";d:0.01000000000000000020816681711721685132943093776702880859375;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:1:{s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:22.71000000000000085265128291212022304534912109375;s:5:\"bytes\";i:1175;s:11:\"size_before\";i:5175;s:10:\"size_after\";i:4000;s:4:\"time\";d:0.01000000000000000020816681711721685132943093776702880859375;}}}'),
	(173,2,'_edit_lock','1531109296:1'),
	(174,2,'title',''),
	(175,2,'subtitle',''),
	(176,2,'waste_type',''),
	(177,2,'image',''),
	(178,2,'_edit_last','1'),
	(179,41,'title',''),
	(180,41,'subtitle',''),
	(181,41,'waste_type',''),
	(182,41,'image',''),
	(183,41,'_edit_last','1'),
	(184,41,'_edit_lock','1531109312:1'),
	(185,43,'title',''),
	(186,43,'subtitle',''),
	(187,43,'waste_type',''),
	(188,43,'image',''),
	(189,43,'_edit_last','1'),
	(190,43,'_edit_lock','1531109330:1'),
	(191,45,'title',''),
	(192,45,'subtitle',''),
	(193,45,'waste_type',''),
	(194,45,'image',''),
	(195,45,'_edit_last','1'),
	(196,45,'_edit_lock','1531109342:1'),
	(197,47,'title',''),
	(198,47,'subtitle',''),
	(199,47,'waste_type',''),
	(200,47,'image',''),
	(201,47,'_edit_last','1'),
	(202,47,'_edit_lock','1531109356:1'),
	(203,49,'title',''),
	(204,49,'subtitle',''),
	(205,49,'waste_type',''),
	(206,49,'image',''),
	(207,49,'_edit_last','1'),
	(208,49,'_edit_lock','1531109379:1'),
	(213,51,'_edit_last','1'),
	(214,51,'_edit_lock','1531206086:1'),
	(215,53,'title',''),
	(216,53,'subtitle',''),
	(217,53,'waste_type',''),
	(218,53,'image',''),
	(219,53,'_edit_last','1'),
	(220,53,'_edit_lock','1531109408:1'),
	(221,55,'title',''),
	(222,55,'subtitle',''),
	(223,55,'waste_type',''),
	(224,55,'image',''),
	(225,55,'_edit_last','1'),
	(226,55,'_edit_lock','1531109427:1'),
	(227,57,'title',''),
	(228,57,'subtitle',''),
	(229,57,'waste_type',''),
	(230,57,'image',''),
	(231,57,'_edit_last','1'),
	(232,57,'_edit_lock','1531109449:1'),
	(233,59,'title',''),
	(234,59,'subtitle',''),
	(235,59,'waste_type',''),
	(236,59,'image',''),
	(237,59,'_edit_last','1'),
	(238,59,'_edit_lock','1531109472:1'),
	(239,61,'title',''),
	(240,61,'subtitle',''),
	(241,61,'waste_type',''),
	(242,61,'image',''),
	(243,61,'_edit_last','1'),
	(244,61,'_edit_lock','1531109486:1'),
	(245,63,'_menu_item_type','post_type'),
	(246,63,'_menu_item_menu_item_parent','0'),
	(247,63,'_menu_item_object_id','49'),
	(248,63,'_menu_item_object','page'),
	(249,63,'_menu_item_target',''),
	(250,63,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(251,63,'_menu_item_xfn',''),
	(252,63,'_menu_item_url',''),
	(254,64,'_menu_item_type','post_type'),
	(255,64,'_menu_item_menu_item_parent','0'),
	(256,64,'_menu_item_object_id','47'),
	(257,64,'_menu_item_object','page'),
	(258,64,'_menu_item_target',''),
	(259,64,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(260,64,'_menu_item_xfn',''),
	(261,64,'_menu_item_url',''),
	(263,65,'_menu_item_type','post_type'),
	(264,65,'_menu_item_menu_item_parent','0'),
	(265,65,'_menu_item_object_id','45'),
	(266,65,'_menu_item_object','page'),
	(267,65,'_menu_item_target',''),
	(268,65,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(269,65,'_menu_item_xfn',''),
	(270,65,'_menu_item_url',''),
	(272,66,'_menu_item_type','post_type'),
	(273,66,'_menu_item_menu_item_parent','0'),
	(274,66,'_menu_item_object_id','43'),
	(275,66,'_menu_item_object','page'),
	(276,66,'_menu_item_target',''),
	(277,66,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(278,66,'_menu_item_xfn',''),
	(279,66,'_menu_item_url',''),
	(281,67,'_menu_item_type','post_type'),
	(282,67,'_menu_item_menu_item_parent','0'),
	(283,67,'_menu_item_object_id','41'),
	(284,67,'_menu_item_object','page'),
	(285,67,'_menu_item_target',''),
	(286,67,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(287,67,'_menu_item_xfn',''),
	(288,67,'_menu_item_url',''),
	(290,68,'_menu_item_type','post_type'),
	(291,68,'_menu_item_menu_item_parent','0'),
	(292,68,'_menu_item_object_id','2'),
	(293,68,'_menu_item_object','page'),
	(294,68,'_menu_item_target',''),
	(295,68,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(296,68,'_menu_item_xfn',''),
	(297,68,'_menu_item_url',''),
	(299,69,'_menu_item_type','post_type'),
	(300,69,'_menu_item_menu_item_parent','0'),
	(301,69,'_menu_item_object_id','61'),
	(302,69,'_menu_item_object','page'),
	(303,69,'_menu_item_target',''),
	(304,69,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(305,69,'_menu_item_xfn',''),
	(306,69,'_menu_item_url',''),
	(308,70,'_menu_item_type','post_type'),
	(309,70,'_menu_item_menu_item_parent','0'),
	(310,70,'_menu_item_object_id','59'),
	(311,70,'_menu_item_object','page'),
	(312,70,'_menu_item_target',''),
	(313,70,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(314,70,'_menu_item_xfn',''),
	(315,70,'_menu_item_url',''),
	(317,71,'_menu_item_type','post_type'),
	(318,71,'_menu_item_menu_item_parent','0'),
	(319,71,'_menu_item_object_id','57'),
	(320,71,'_menu_item_object','page'),
	(321,71,'_menu_item_target',''),
	(322,71,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(323,71,'_menu_item_xfn',''),
	(324,71,'_menu_item_url',''),
	(326,72,'_menu_item_type','post_type'),
	(327,72,'_menu_item_menu_item_parent','0'),
	(328,72,'_menu_item_object_id','55'),
	(329,72,'_menu_item_object','page'),
	(330,72,'_menu_item_target',''),
	(331,72,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(332,72,'_menu_item_xfn',''),
	(333,72,'_menu_item_url',''),
	(335,73,'_menu_item_type','post_type'),
	(336,73,'_menu_item_menu_item_parent','0'),
	(337,73,'_menu_item_object_id','53'),
	(338,73,'_menu_item_object','page'),
	(339,73,'_menu_item_target',''),
	(340,73,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(341,73,'_menu_item_xfn',''),
	(342,73,'_menu_item_url',''),
	(344,74,'_menu_item_type','post_type'),
	(345,74,'_menu_item_menu_item_parent','0'),
	(346,74,'_menu_item_object_id','51'),
	(347,74,'_menu_item_object','page'),
	(348,74,'_menu_item_target',''),
	(349,74,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(350,74,'_menu_item_xfn',''),
	(351,74,'_menu_item_url',''),
	(353,75,'_menu_item_type','post_type'),
	(354,75,'_menu_item_menu_item_parent','0'),
	(355,75,'_menu_item_object_id','41'),
	(356,75,'_menu_item_object','page'),
	(357,75,'_menu_item_target',''),
	(358,75,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(359,75,'_menu_item_xfn',''),
	(360,75,'_menu_item_url',''),
	(362,76,'_menu_item_type','post_type'),
	(363,76,'_menu_item_menu_item_parent','0'),
	(364,76,'_menu_item_object_id','43'),
	(365,76,'_menu_item_object','page'),
	(366,76,'_menu_item_target',''),
	(367,76,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(368,76,'_menu_item_xfn',''),
	(369,76,'_menu_item_url',''),
	(371,77,'_menu_item_type','post_type'),
	(372,77,'_menu_item_menu_item_parent','0'),
	(373,77,'_menu_item_object_id','47'),
	(374,77,'_menu_item_object','page'),
	(375,77,'_menu_item_target',''),
	(376,77,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(377,77,'_menu_item_xfn',''),
	(378,77,'_menu_item_url',''),
	(380,78,'_menu_item_type','post_type'),
	(381,78,'_menu_item_menu_item_parent','0'),
	(382,78,'_menu_item_object_id','49'),
	(383,78,'_menu_item_object','page'),
	(384,78,'_menu_item_target',''),
	(385,78,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(386,78,'_menu_item_xfn',''),
	(387,78,'_menu_item_url',''),
	(389,79,'_menu_item_type','post_type'),
	(390,79,'_menu_item_menu_item_parent','0'),
	(391,79,'_menu_item_object_id','6'),
	(392,79,'_menu_item_object','page'),
	(393,79,'_menu_item_target',''),
	(394,79,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(395,79,'_menu_item_xfn',''),
	(396,79,'_menu_item_url',''),
	(398,80,'_menu_item_type','post_type'),
	(399,80,'_menu_item_menu_item_parent','0'),
	(400,80,'_menu_item_object_id','51'),
	(401,80,'_menu_item_object','page'),
	(402,80,'_menu_item_target',''),
	(403,80,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(404,80,'_menu_item_xfn',''),
	(405,80,'_menu_item_url',''),
	(407,81,'_menu_item_type','post_type'),
	(408,81,'_menu_item_menu_item_parent','0'),
	(409,81,'_menu_item_object_id','53'),
	(410,81,'_menu_item_object','page'),
	(411,81,'_menu_item_target',''),
	(412,81,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(413,81,'_menu_item_xfn',''),
	(414,81,'_menu_item_url',''),
	(416,82,'_menu_item_type','post_type'),
	(417,82,'_menu_item_menu_item_parent','0'),
	(418,82,'_menu_item_object_id','61'),
	(419,82,'_menu_item_object','page'),
	(420,82,'_menu_item_target',''),
	(421,82,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(422,82,'_menu_item_xfn',''),
	(423,82,'_menu_item_url',''),
	(425,83,'_menu_item_type','post_type'),
	(426,83,'_menu_item_menu_item_parent','0'),
	(427,83,'_menu_item_object_id','45'),
	(428,83,'_menu_item_object','page'),
	(429,83,'_menu_item_target',''),
	(430,83,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(431,83,'_menu_item_xfn',''),
	(432,83,'_menu_item_url',''),
	(434,84,'_wp_attached_file','2018/07/banner-page.jpg'),
	(435,84,'_wp_attachment_metadata','a:5:{s:5:\"width\";s:4:\"1513\";s:6:\"height\";s:3:\"370\";s:4:\"file\";s:23:\"2018/07/banner-page.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"banner-page-150x150.jpg\";s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"banner-page-300x73.jpg\";s:5:\"width\";s:3:\"300\";s:6:\"height\";s:2:\"73\";s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"banner-page-768x188.jpg\";s:5:\"width\";s:3:\"768\";s:6:\"height\";s:3:\"188\";s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"banner-page-1024x250.jpg\";s:5:\"width\";s:4:\"1024\";s:6:\"height\";s:3:\"250\";s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
	(442,84,'wp-smpro-smush-data','a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:4.3775751122633206335876820958219468593597412109375;s:5:\"bytes\";i:4728;s:11:\"size_before\";i:108005;s:10:\"size_after\";i:103277;s:4:\"time\";d:0.070000000000000006661338147750939242541790008544921875;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:4:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.79000000000000003552713678800500929355621337890625;s:5:\"bytes\";i:450;s:11:\"size_before\";i:7768;s:10:\"size_after\";i:7318;s:4:\"time\";d:0.0200000000000000004163336342344337026588618755340576171875;}s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.45000000000000017763568394002504646778106689453125;s:5:\"bytes\";i:537;s:11:\"size_before\";i:8332;s:10:\"size_after\";i:7795;s:4:\"time\";d:0.01000000000000000020816681711721685132943093776702880859375;}s:12:\"medium_large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.61000000000000031974423109204508364200592041015625;s:5:\"bytes\";i:1661;s:11:\"size_before\";i:36056;s:10:\"size_after\";i:34395;s:4:\"time\";d:0.0200000000000000004163336342344337026588618755340576171875;}s:5:\"large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.720000000000000195399252334027551114559173583984375;s:5:\"bytes\";i:2080;s:11:\"size_before\";i:55849;s:10:\"size_after\";i:53769;s:4:\"time\";d:0.0200000000000000004163336342344337026588618755340576171875;}}}'),
	(447,51,'image','84'),
	(448,51,'image_text',''),
	(449,51,'title',''),
	(450,51,'subtitle',''),
	(451,51,'waste_type',''),
	(452,51,'image','');

/*!40000 ALTER TABLE `ws_postmeta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ws_posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ws_posts`;

CREATE TABLE `ws_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `ws_posts` WRITE;
/*!40000 ALTER TABLE `ws_posts` DISABLE KEYS */;

INSERT INTO `ws_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`)
VALUES
	(1,1,'2018-07-03 07:37:37','2018-07-03 07:37:37','Welcome to WordPress. This is your first post. Edit or delete it, then start writing!','Hello world!','','publish','open','open','','hello-world','','','2018-07-03 07:37:37','2018-07-03 07:37:37','',0,'http://waskips:8888/?p=1',0,'post','',1),
	(2,1,'2018-07-03 07:37:37','2018-07-03 07:37:37','This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\r\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</blockquote>\r\n...or something like this:\r\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\r\nAs a new WordPress user, you should go to <a href=\"http://waskips:8888/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!','My Account','','publish','closed','open','','my-account','','','2018-07-09 04:10:35','2018-07-09 04:10:35','',0,'http://waskips:8888/?page_id=2',0,'page','',0),
	(3,1,'2018-07-03 07:37:37','2018-07-03 07:37:37','<h2>Who we are</h2><p>Our website address is: http://waskips:8888.</p><h2>What personal data we collect and why we collect it</h2><h3>Comments</h3><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><h3>Media</h3><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><h3>Contact forms</h3><h3>Cookies</h3><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><h3>Embedded content from other websites</h3><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracing your interaction with the embedded content if you have an account and are logged in to that website.</p><h3>Analytics</h3><h2>Who we share your data with</h2><h2>How long we retain your data</h2><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><h2>What rights you have over your data</h2><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><h2>Where we send your data</h2><p>Visitor comments may be checked through an automated spam detection service.</p><h2>Your contact information</h2><h2>Additional information</h2><h3>How we protect your data</h3><h3>What data breach procedures we have in place</h3><h3>What third parties we receive data from</h3><h3>What automated decision making and/or profiling we do with user data</h3><h3>Industry regulatory disclosure requirements</h3>','Privacy Policy','','draft','closed','open','','privacy-policy','','','2018-07-03 07:37:37','2018-07-03 07:37:37','',0,'http://waskips:8888/?page_id=3',0,'page','',0),
	(5,1,'2018-07-04 00:52:47','2018-07-04 00:52:47','<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit \"Send\"]\nWA Skips \"[your-subject]\"\n[your-name] <wordpress@waskips>\nFrom: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on WA Skips (http://waskips:8888)\nachmad.bcodes@gmail.com\nReply-To: [your-email]\n\n0\n0\n\nWA Skips \"[your-subject]\"\nWA Skips <wordpress@waskips>\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on WA Skips (http://waskips:8888)\n[your-email]\nReply-To: achmad.bcodes@gmail.com\n\n0\n0\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.','Contact form 1','','publish','closed','closed','','contact-form-1','','','2018-07-04 00:52:47','2018-07-04 00:52:47','',0,'http://waskips:8888/?post_type=wpcf7_contact_form&p=5',0,'wpcf7_contact_form','',0),
	(6,1,'2018-07-04 01:16:09','2018-07-04 01:16:09','','Home','','publish','closed','closed','','home','','','2018-07-09 00:32:04','2018-07-09 00:32:04','',0,'http://waskips:8888/?page_id=6',0,'page','',0),
	(7,1,'2018-07-04 01:16:09','2018-07-04 01:16:09','','Home','','inherit','closed','closed','','6-revision-v1','','','2018-07-04 01:16:09','2018-07-04 01:16:09','',6,'http://waskips:8888/2018/07/04/6-revision-v1/',0,'revision','',0),
	(9,1,'2018-07-04 02:52:01','2018-07-04 02:52:01',' ','','','publish','closed','closed','','9','','','2018-07-09 07:23:37','2018-07-09 07:23:37','',0,'http://waskips:8888/?p=9',1,'nav_menu_item','',0),
	(10,1,'2018-07-04 04:26:39','2018-07-04 04:26:39','','Wa-skips-logo','','inherit','open','closed','','wa-skips-logo','','','2018-07-04 04:26:39','2018-07-04 04:26:39','',0,'http://waskips:8888/wp-content/uploads/2018/07/Wa-skips-logo.png',0,'attachment','image/png',0),
	(11,1,'2018-07-05 05:23:13','2018-07-05 05:23:13','','call-button-2','','inherit','open','closed','','call-button-2','','','2018-07-05 05:23:13','2018-07-05 05:23:13','',0,'http://waskips:8888/wp-content/uploads/2018/07/call-button-2.png',0,'attachment','image/png',0),
	(12,1,'2018-07-05 06:14:05','2018-07-05 06:14:05','','Image Slider','','publish','closed','closed','','image-slider','','','2018-07-05 06:14:05','2018-07-05 06:14:05','',0,'http://waskips:8888/cfs/image-slider/',0,'cfs','',0),
	(13,1,'2018-07-05 06:14:05','2018-07-05 06:14:05','','Text Info','','publish','closed','closed','','text-intro','','','2018-07-09 00:11:43','2018-07-09 00:11:43','',0,'http://waskips:8888/cfs/text-intro/',0,'cfs','',0),
	(14,1,'2018-07-05 06:14:05','2018-07-05 06:14:05','','Page Banner','','publish','closed','closed','','page-banner','','','2018-07-05 06:14:05','2018-07-05 06:14:05','',0,'http://waskips:8888/cfs/page-banner/',0,'cfs','',0),
	(15,1,'2018-07-05 06:14:05','2018-07-05 06:14:05','','Tour Attributes','','publish','closed','closed','','price','','','2018-07-05 06:14:05','2018-07-05 06:14:05','',0,'http://waskips:8888/cfs/price/',0,'cfs','',0),
	(17,1,'2018-07-06 00:45:40','2018-07-06 00:45:40','','slider-1','','inherit','open','closed','','slider-1','','','2018-07-06 00:45:40','2018-07-06 00:45:40','',6,'http://waskips:8888/wp-content/uploads/2018/07/slider-1.jpg',0,'attachment','image/jpeg',0),
	(18,1,'2018-07-08 14:49:36','2018-07-08 14:49:36','','1','','inherit','open','closed','','1','','','2018-07-08 14:49:36','2018-07-08 14:49:36','',0,'http://waskips:8888/wp-content/uploads/2018/07/1.png',0,'attachment','image/png',0),
	(19,1,'2018-07-08 14:49:38','2018-07-08 14:49:38','','2','','inherit','open','closed','','2','','','2018-07-08 14:49:38','2018-07-08 14:49:38','',0,'http://waskips:8888/wp-content/uploads/2018/07/2.png',0,'attachment','image/png',0),
	(20,1,'2018-07-08 14:49:39','2018-07-08 14:49:39','','3','','inherit','open','closed','','3','','','2018-07-08 14:49:39','2018-07-08 14:49:39','',0,'http://waskips:8888/wp-content/uploads/2018/07/3.png',0,'attachment','image/png',0),
	(21,1,'2018-07-08 14:49:41','2018-07-08 14:49:41','','4','','inherit','open','closed','','4','','','2018-07-08 14:49:41','2018-07-08 14:49:41','',0,'http://waskips:8888/wp-content/uploads/2018/07/4.png',0,'attachment','image/png',0),
	(22,1,'2018-07-08 14:49:43','2018-07-08 14:49:43','','5','','inherit','open','closed','','5','','','2018-07-08 14:49:43','2018-07-08 14:49:43','',0,'http://waskips:8888/wp-content/uploads/2018/07/5.png',0,'attachment','image/png',0),
	(23,1,'2018-07-08 14:49:45','2018-07-08 14:49:45','','6','','inherit','open','closed','','6','','','2018-07-08 14:49:45','2018-07-08 14:49:45','',0,'http://waskips:8888/wp-content/uploads/2018/07/6.png',0,'attachment','image/png',0),
	(24,1,'2018-07-08 14:49:48','2018-07-08 14:49:48','','7','','inherit','open','closed','','7','','','2018-07-08 14:49:48','2018-07-08 14:49:48','',0,'http://waskips:8888/wp-content/uploads/2018/07/7.png',0,'attachment','image/png',0),
	(25,1,'2018-07-08 14:49:50','2018-07-08 14:49:50','','8','','inherit','open','closed','','8-2','','','2018-07-08 14:49:50','2018-07-08 14:49:50','',0,'http://waskips:8888/wp-content/uploads/2018/07/8.png',0,'attachment','image/png',0),
	(26,1,'2018-07-08 14:49:52','2018-07-08 14:49:52','','a','','inherit','open','closed','','a','','','2018-07-08 14:49:52','2018-07-08 14:49:52','',0,'http://waskips:8888/wp-content/uploads/2018/07/a.png',0,'attachment','image/png',0),
	(27,1,'2018-07-08 14:49:54','2018-07-08 14:49:54','','b','','inherit','open','closed','','b','','','2018-07-08 14:49:54','2018-07-08 14:49:54','',0,'http://waskips:8888/wp-content/uploads/2018/07/b.png',0,'attachment','image/png',0),
	(28,1,'2018-07-08 14:49:56','2018-07-08 14:49:56','','c','','inherit','open','closed','','c','','','2018-07-08 14:49:56','2018-07-08 14:49:56','',0,'http://waskips:8888/wp-content/uploads/2018/07/c.png',0,'attachment','image/png',0),
	(29,1,'2018-07-08 14:49:58','2018-07-08 14:49:58','','d-active','','inherit','open','closed','','d-active','','','2018-07-08 14:49:58','2018-07-08 14:49:58','',0,'http://waskips:8888/wp-content/uploads/2018/07/d-active.png',0,'attachment','image/png',0),
	(30,1,'2018-07-08 14:49:59','2018-07-08 14:49:59','','d','','inherit','open','closed','','d','','','2018-07-08 14:49:59','2018-07-08 14:49:59','',0,'http://waskips:8888/wp-content/uploads/2018/07/d.png',0,'attachment','image/png',0),
	(31,1,'2018-07-08 14:50:01','2018-07-08 14:50:01','','e-active','','inherit','open','closed','','e-active','','','2018-07-08 14:50:01','2018-07-08 14:50:01','',0,'http://waskips:8888/wp-content/uploads/2018/07/e-active.png',0,'attachment','image/png',0),
	(32,1,'2018-07-08 14:50:03','2018-07-08 14:50:03','','e','','inherit','open','closed','','e','','','2018-07-08 14:50:03','2018-07-08 14:50:03','',0,'http://waskips:8888/wp-content/uploads/2018/07/e.png',0,'attachment','image/png',0),
	(33,1,'2018-07-08 14:50:05','2018-07-08 14:50:05','','f-active','','inherit','open','closed','','f-active','','','2018-07-08 14:50:05','2018-07-08 14:50:05','',0,'http://waskips:8888/wp-content/uploads/2018/07/f-active.png',0,'attachment','image/png',0),
	(34,1,'2018-07-08 14:50:07','2018-07-08 14:50:07','','f','','inherit','open','closed','','f','','','2018-07-08 14:50:07','2018-07-08 14:50:07','',0,'http://waskips:8888/wp-content/uploads/2018/07/f.png',0,'attachment','image/png',0),
	(36,1,'2018-07-08 14:50:11','2018-07-08 14:50:11','','phone','','inherit','open','closed','','phone','','','2018-07-08 14:50:11','2018-07-08 14:50:11','',0,'http://waskips:8888/wp-content/uploads/2018/07/phone.png',0,'attachment','image/png',0),
	(38,1,'2018-07-09 00:05:14','2018-07-09 00:05:14','','Text Info','','inherit','closed','closed','','13-autosave-v1','','','2018-07-09 00:05:14','2018-07-09 00:05:14','',13,'http://waskips:8888/13-autosave-v1/',0,'revision','',0),
	(39,1,'2018-07-09 04:00:38','2018-07-09 04:00:38','','payment-icon','','inherit','open','closed','','payment-icon','','','2018-07-09 04:00:38','2018-07-09 04:00:38','',0,'http://waskips:8888/wp-content/uploads/2018/07/payment-icon.png',0,'attachment','image/png',0),
	(40,1,'2018-07-09 04:10:35','2018-07-09 04:10:35','This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\r\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</blockquote>\r\n...or something like this:\r\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\r\nAs a new WordPress user, you should go to <a href=\"http://waskips:8888/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!','My Account','','inherit','closed','closed','','2-revision-v1','','','2018-07-09 04:10:35','2018-07-09 04:10:35','',2,'http://waskips:8888/2-revision-v1/',0,'revision','',0),
	(41,1,'2018-07-09 04:10:51','2018-07-09 04:10:51','','Instruction','','publish','closed','closed','','instruction','','','2018-07-09 04:10:51','2018-07-09 04:10:51','',0,'http://waskips:8888/?page_id=41',0,'page','',0),
	(42,1,'2018-07-09 04:10:51','2018-07-09 04:10:51','','Instruction','','inherit','closed','closed','','41-revision-v1','','','2018-07-09 04:10:51','2018-07-09 04:10:51','',41,'http://waskips:8888/41-revision-v1/',0,'revision','',0),
	(43,1,'2018-07-09 04:11:07','2018-07-09 04:11:07','','About Us','','publish','closed','closed','','about-us','','','2018-07-09 04:11:07','2018-07-09 04:11:07','',0,'http://waskips:8888/?page_id=43',0,'page','',0),
	(44,1,'2018-07-09 04:11:07','2018-07-09 04:11:07','','About Us','','inherit','closed','closed','','43-revision-v1','','','2018-07-09 04:11:07','2018-07-09 04:11:07','',43,'http://waskips:8888/43-revision-v1/',0,'revision','',0),
	(45,1,'2018-07-09 04:11:20','2018-07-09 04:11:20','','FAQ','','publish','closed','closed','','faq','','','2018-07-09 04:11:20','2018-07-09 04:11:20','',0,'http://waskips:8888/?page_id=45',0,'page','',0),
	(46,1,'2018-07-09 04:11:20','2018-07-09 04:11:20','','FAQ','','inherit','closed','closed','','45-revision-v1','','','2018-07-09 04:11:20','2018-07-09 04:11:20','',45,'http://waskips:8888/45-revision-v1/',0,'revision','',0),
	(47,1,'2018-07-09 04:11:31','2018-07-09 04:11:31','','Blog','','publish','closed','closed','','blog','','','2018-07-09 04:11:31','2018-07-09 04:11:31','',0,'http://waskips:8888/?page_id=47',0,'page','',0),
	(48,1,'2018-07-09 04:11:31','2018-07-09 04:11:31','','Blog','','inherit','closed','closed','','47-revision-v1','','','2018-07-09 04:11:31','2018-07-09 04:11:31','',47,'http://waskips:8888/47-revision-v1/',0,'revision','',0),
	(49,1,'2018-07-09 04:11:46','2018-07-09 04:11:46','','Contact Us','','publish','closed','closed','','contact-us','','','2018-07-09 04:11:46','2018-07-09 04:11:46','',0,'http://waskips:8888/?page_id=49',0,'page','',0),
	(50,1,'2018-07-09 04:11:46','2018-07-09 04:11:46','','Contact Us','','inherit','closed','closed','','49-revision-v1','','','2018-07-09 04:11:46','2018-07-09 04:11:46','',49,'http://waskips:8888/49-revision-v1/',0,'revision','',0),
	(51,1,'2018-07-09 04:12:09','2018-07-09 04:12:09','','Skip Bin Order','','publish','closed','closed','','skip-bin-order','','','2018-07-10 07:01:26','2018-07-10 07:01:26','',0,'http://waskips:8888/?page_id=51',0,'page','',0),
	(52,1,'2018-07-09 04:12:09','2018-07-09 04:12:09','','Skip Bin Order','','inherit','closed','closed','','51-revision-v1','','','2018-07-09 04:12:09','2018-07-09 04:12:09','',51,'http://waskips:8888/51-revision-v1/',0,'revision','',0),
	(53,1,'2018-07-09 04:12:24','2018-07-09 04:12:24','','Skip Types & Sizes','','publish','closed','closed','','skip-types-sizes','','','2018-07-09 04:12:24','2018-07-09 04:12:24','',0,'http://waskips:8888/?page_id=53',0,'page','',0),
	(54,1,'2018-07-09 04:12:24','2018-07-09 04:12:24','','Skip Types & Sizes','','inherit','closed','closed','','53-revision-v1','','','2018-07-09 04:12:24','2018-07-09 04:12:24','',53,'http://waskips:8888/53-revision-v1/',0,'revision','',0),
	(55,1,'2018-07-09 04:12:38','2018-07-09 04:12:38','','Bin Filling','','publish','closed','closed','','bin-filling','','','2018-07-09 04:12:38','2018-07-09 04:12:38','',0,'http://waskips:8888/?page_id=55',0,'page','',0),
	(56,1,'2018-07-09 04:12:38','2018-07-09 04:12:38','','Bin Filling','','inherit','closed','closed','','55-revision-v1','','','2018-07-09 04:12:38','2018-07-09 04:12:38','',55,'http://waskips:8888/55-revision-v1/',0,'revision','',0),
	(57,1,'2018-07-09 04:12:57','2018-07-09 04:12:57','','Service Area','','publish','closed','closed','','service-area','','','2018-07-09 04:12:57','2018-07-09 04:12:57','',0,'http://waskips:8888/?page_id=57',0,'page','',0),
	(58,1,'2018-07-09 04:12:57','2018-07-09 04:12:57','','Service Area','','inherit','closed','closed','','57-revision-v1','','','2018-07-09 04:12:57','2018-07-09 04:12:57','',57,'http://waskips:8888/57-revision-v1/',0,'revision','',0),
	(59,1,'2018-07-09 04:13:19','2018-07-09 04:13:19','','Recycling','','publish','closed','closed','','recycling','','','2018-07-09 04:13:19','2018-07-09 04:13:19','',0,'http://waskips:8888/?page_id=59',0,'page','',0),
	(60,1,'2018-07-09 04:13:19','2018-07-09 04:13:19','','Recycling','','inherit','closed','closed','','59-revision-v1','','','2018-07-09 04:13:19','2018-07-09 04:13:19','',59,'http://waskips:8888/59-revision-v1/',0,'revision','',0),
	(61,1,'2018-07-09 04:13:43','2018-07-09 04:13:43','','Hazardous Goods','','publish','closed','closed','','hazardous-goods','','','2018-07-09 04:13:43','2018-07-09 04:13:43','',0,'http://waskips:8888/?page_id=61',0,'page','',0),
	(62,1,'2018-07-09 04:13:43','2018-07-09 04:13:43','','Hazardous Goods','','inherit','closed','closed','','61-revision-v1','','','2018-07-09 04:13:43','2018-07-09 04:13:43','',61,'http://waskips:8888/61-revision-v1/',0,'revision','',0),
	(63,1,'2018-07-09 04:14:19','2018-07-09 04:14:19',' ','','','publish','closed','closed','','63','','','2018-07-09 04:14:19','2018-07-09 04:14:19','',0,'http://waskips:8888/?p=63',6,'nav_menu_item','',0),
	(64,1,'2018-07-09 04:14:19','2018-07-09 04:14:19',' ','','','publish','closed','closed','','64','','','2018-07-09 04:14:19','2018-07-09 04:14:19','',0,'http://waskips:8888/?p=64',5,'nav_menu_item','',0),
	(65,1,'2018-07-09 04:14:19','2018-07-09 04:14:19',' ','','','publish','closed','closed','','65','','','2018-07-09 04:14:19','2018-07-09 04:14:19','',0,'http://waskips:8888/?p=65',4,'nav_menu_item','',0),
	(66,1,'2018-07-09 04:14:19','2018-07-09 04:14:19',' ','','','publish','closed','closed','','66','','','2018-07-09 04:14:19','2018-07-09 04:14:19','',0,'http://waskips:8888/?p=66',3,'nav_menu_item','',0),
	(67,1,'2018-07-09 04:14:19','2018-07-09 04:14:19',' ','','','publish','closed','closed','','67','','','2018-07-09 04:14:19','2018-07-09 04:14:19','',0,'http://waskips:8888/?p=67',2,'nav_menu_item','',0),
	(68,1,'2018-07-09 04:14:19','2018-07-09 04:14:19',' ','','','publish','closed','closed','','68','','','2018-07-09 04:14:19','2018-07-09 04:14:19','',0,'http://waskips:8888/?p=68',1,'nav_menu_item','',0),
	(69,1,'2018-07-09 04:15:05','2018-07-09 04:15:05',' ','','','publish','closed','closed','','69','','','2018-07-09 04:15:05','2018-07-09 04:15:05','',0,'http://waskips:8888/?p=69',6,'nav_menu_item','',0),
	(70,1,'2018-07-09 04:15:05','2018-07-09 04:15:05',' ','','','publish','closed','closed','','70','','','2018-07-09 04:15:05','2018-07-09 04:15:05','',0,'http://waskips:8888/?p=70',5,'nav_menu_item','',0),
	(71,1,'2018-07-09 04:15:05','2018-07-09 04:15:05',' ','','','publish','closed','closed','','71','','','2018-07-09 04:15:05','2018-07-09 04:15:05','',0,'http://waskips:8888/?p=71',4,'nav_menu_item','',0),
	(72,1,'2018-07-09 04:15:05','2018-07-09 04:15:05',' ','','','publish','closed','closed','','72','','','2018-07-09 04:15:05','2018-07-09 04:15:05','',0,'http://waskips:8888/?p=72',3,'nav_menu_item','',0),
	(73,1,'2018-07-09 04:15:05','2018-07-09 04:15:05',' ','','','publish','closed','closed','','73','','','2018-07-09 04:15:05','2018-07-09 04:15:05','',0,'http://waskips:8888/?p=73',2,'nav_menu_item','',0),
	(74,1,'2018-07-09 04:15:05','2018-07-09 04:15:05',' ','','','publish','closed','closed','','74','','','2018-07-09 04:15:05','2018-07-09 04:15:05','',0,'http://waskips:8888/?p=74',1,'nav_menu_item','',0),
	(75,1,'2018-07-09 07:23:37','2018-07-09 07:23:37',' ','','','publish','closed','closed','','75','','','2018-07-09 07:23:37','2018-07-09 07:23:37','',0,'http://waskips:8888/?p=75',2,'nav_menu_item','',0),
	(76,1,'2018-07-09 07:23:37','2018-07-09 07:23:37',' ','','','publish','closed','closed','','76','','','2018-07-09 07:23:37','2018-07-09 07:23:37','',0,'http://waskips:8888/?p=76',3,'nav_menu_item','',0),
	(77,1,'2018-07-09 07:23:37','2018-07-09 07:23:37',' ','','','publish','closed','closed','','77','','','2018-07-09 07:23:37','2018-07-09 07:23:37','',0,'http://waskips:8888/?p=77',4,'nav_menu_item','',0),
	(78,1,'2018-07-09 07:23:37','2018-07-09 07:23:37',' ','','','publish','closed','closed','','78','','','2018-07-09 07:23:37','2018-07-09 07:23:37','',0,'http://waskips:8888/?p=78',5,'nav_menu_item','',0),
	(79,1,'2018-07-09 07:24:28','2018-07-09 07:24:28',' ','','','publish','closed','closed','','79','','','2018-07-09 07:24:28','2018-07-09 07:24:28','',0,'http://waskips:8888/?p=79',1,'nav_menu_item','',0),
	(80,1,'2018-07-09 07:24:28','2018-07-09 07:24:28',' ','','','publish','closed','closed','','80','','','2018-07-09 07:24:28','2018-07-09 07:24:28','',0,'http://waskips:8888/?p=80',2,'nav_menu_item','',0),
	(81,1,'2018-07-09 07:24:28','2018-07-09 07:24:28',' ','','','publish','closed','closed','','81','','','2018-07-09 07:24:28','2018-07-09 07:24:28','',0,'http://waskips:8888/?p=81',3,'nav_menu_item','',0),
	(82,1,'2018-07-09 07:24:28','2018-07-09 07:24:28',' ','','','publish','closed','closed','','82','','','2018-07-09 07:24:28','2018-07-09 07:24:28','',0,'http://waskips:8888/?p=82',4,'nav_menu_item','',0),
	(83,1,'2018-07-09 07:24:28','2018-07-09 07:24:28',' ','','','publish','closed','closed','','83','','','2018-07-09 07:24:28','2018-07-09 07:24:28','',0,'http://waskips:8888/?p=83',5,'nav_menu_item','',0),
	(84,1,'2018-07-10 06:43:41','2018-07-10 06:43:41','','banner-page','','inherit','open','closed','','banner-page','','','2018-07-10 06:43:41','2018-07-10 06:43:41','',51,'http://waskips:8888/wp-content/uploads/2018/07/banner-page.jpg',0,'attachment','image/jpeg',0);

/*!40000 ALTER TABLE `ws_posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ws_term_relationships
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ws_term_relationships`;

CREATE TABLE `ws_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `ws_term_relationships` WRITE;
/*!40000 ALTER TABLE `ws_term_relationships` DISABLE KEYS */;

INSERT INTO `ws_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`)
VALUES
	(1,1,0),
	(9,2,0),
	(63,3,0),
	(64,3,0),
	(65,3,0),
	(66,3,0),
	(67,3,0),
	(68,3,0),
	(69,4,0),
	(70,4,0),
	(71,4,0),
	(72,4,0),
	(73,4,0),
	(74,4,0),
	(75,2,0),
	(76,2,0),
	(77,2,0),
	(78,2,0),
	(79,5,0),
	(80,5,0),
	(81,5,0),
	(82,5,0),
	(83,5,0);

/*!40000 ALTER TABLE `ws_term_relationships` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ws_term_taxonomy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ws_term_taxonomy`;

CREATE TABLE `ws_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `ws_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `ws_term_taxonomy` DISABLE KEYS */;

INSERT INTO `ws_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`)
VALUES
	(1,1,'category','',0,1),
	(2,2,'nav_menu','',0,5),
	(3,3,'nav_menu','',0,6),
	(4,4,'nav_menu','',0,6),
	(5,5,'nav_menu','',0,5);

/*!40000 ALTER TABLE `ws_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ws_termmeta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ws_termmeta`;

CREATE TABLE `ws_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table ws_terms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ws_terms`;

CREATE TABLE `ws_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `ws_terms` WRITE;
/*!40000 ALTER TABLE `ws_terms` DISABLE KEYS */;

INSERT INTO `ws_terms` (`term_id`, `name`, `slug`, `term_group`)
VALUES
	(1,'Uncategorized','uncategorized',0),
	(2,'Top menu','top-menu',0),
	(3,'Menu footer 1','menu-footer-1',0),
	(4,'Menu footer 2','menu-footer-2',0),
	(5,'Main Menu','main-menu',0);

/*!40000 ALTER TABLE `ws_terms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ws_usermeta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ws_usermeta`;

CREATE TABLE `ws_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `ws_usermeta` WRITE;
/*!40000 ALTER TABLE `ws_usermeta` DISABLE KEYS */;

INSERT INTO `ws_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`)
VALUES
	(1,1,'nickname','adminws'),
	(2,1,'first_name',''),
	(3,1,'last_name',''),
	(4,1,'description',''),
	(5,1,'rich_editing','true'),
	(6,1,'syntax_highlighting','true'),
	(7,1,'comment_shortcuts','false'),
	(8,1,'admin_color','fresh'),
	(9,1,'use_ssl','0'),
	(10,1,'show_admin_bar_front','false'),
	(11,1,'locale',''),
	(12,1,'ws_capabilities','a:1:{s:13:\"administrator\";b:1;}'),
	(13,1,'ws_user_level','10'),
	(14,1,'dismissed_wp_pointers','wp496_privacy,text_widget_custom_html'),
	(15,1,'show_welcome_panel','1'),
	(16,1,'session_tokens','a:1:{s:64:\"85af4a2d75cda82d44a7efe0004f4be8491f01c34936fef90db78c4248cf6cf6\";a:4:{s:10:\"expiration\";i:1531407783;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36\";s:5:\"login\";i:1531234983;}}'),
	(17,1,'ws_dashboard_quick_press_last_post_id','4'),
	(18,1,'ws_user-settings','editor=tinymce&libraryContent=browse&hidetb=1'),
	(19,1,'ws_user-settings-time','1531095344'),
	(20,1,'managenav-menuscolumnshidden','a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
	(21,1,'metaboxhidden_nav-menus','a:1:{i:0;s:12:\"add-post_tag\";}'),
	(22,1,'nav_menu_recently_edited','5');

/*!40000 ALTER TABLE `ws_usermeta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ws_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ws_users`;

CREATE TABLE `ws_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `ws_users` WRITE;
/*!40000 ALTER TABLE `ws_users` DISABLE KEYS */;

INSERT INTO `ws_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`)
VALUES
	(1,'adminws','$P$BxSlWke6VWOc1f9btjNeLrCqQD3c37.','adminws','achmad.bcodes@gmail.com','','2018-07-03 07:37:37','',0,'adminws');

/*!40000 ALTER TABLE `ws_users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
